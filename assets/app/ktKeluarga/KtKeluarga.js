var KtKeluarga = function () {
	var t_daftar_anggota_kk				= $("#t_daftar_anggota_kk");
	// var t_user_req_ver				= $("#t_user_req_ver");

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	var handle_form = function() {
		$(document).on('click', '#submit_form_kk', function(){
			$("#form_kk").data("validator").settings.submitHandler = function (form, validator) {
				if (confirm("Apakah anda yakin untuk menyimpan data ini ?") == false) {	
						return;
					}
					
					ajax = Dica_app.ajax_func(Dica_app.getPath() + "ktKeluarga/Kt_keluarga/save",
										$('#form_kk').serialize()
										,"json","POST");
											
					ajax.success(function (res) { 
						if (res.status == 'success'){
							$("#form_kk").trigger('reset');
							$("#id").val('');
						
							bootbox.alert(res.message, function(){ 
								setTimeout(function(){ window.open(Dica_app.getPath() + "ktKeluarga/Kt_keluarga", "_self"); }, 100);
							})
							
						}else{
							bootbox.alert(res.message);    
						}	
					});
					ajax.error(function (xhr, ajaxOptions, thrownError) {
						bootbox.alert('error');
					});
				};
		}); 
	};
	
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	var handle_datatable = function() {
		// alert('dicahideto');
		
		$('#t_daftar_kk').DataTable( 
			{
				"scrollX": true,
				"columnDefs": [
					{
						"targets": [ 1,7 ],
						"visible": false,
						"searchable": false,
					}
				]
			} );
			
		$('#t_daftar_kk tbody').on('click', '.view', function () {
				   var table = $('#t_daftar_kk').DataTable();
				   var data = table.row($(this).closest('tr')).data();
				  
				  
				var aData1 = data[Object.keys(data)[1]];
				  var aData2 = data[Object.keys(data)[2]];
				  var aData3 = data[Object.keys(data)[3]];
				  var aData4 = data[Object.keys(data)[4]];
				  var aData5 = data[Object.keys(data)[5]];
				  var aData6 = data[Object.keys(data)[6]];
				  var aData7 = data[Object.keys(data)[7]];
				 
				 $('#id').val(aData1);
				 $('#no_kk').val(aData2);
				 $('#kelurahan').val(aData7).change();

				 $('#alamat').val(aData4);
				 $('#latitude').val(aData5);
				 $('#longitude').val(aData6);
				  // // alert(aData1);
				  $("#actions").hide();
				  t_daftar_anggota_kk.api().ajax.url(Dica_app.getPath() + "ktKeluarga/Kt_keluarga/select_anggota_kk?kk=" + aData2).load();
				  // alert("ktKeluarga/Kt_keluarga/select_anggota_kk?kk=" + aData2);
			 });
		
		$('#t_daftar_kk tbody').on('click', '.edit', function () {
				   var table = $('#t_daftar_kk').DataTable();
				   var data = table.row($(this).closest('tr')).data();
				   $('#code').val(data[Object.keys(data)[1]]);
				  
				  var aData1 = data[Object.keys(data)[1]];
				  var aData2 = data[Object.keys(data)[2]];
				  var aData3 = data[Object.keys(data)[3]];
				  var aData4 = data[Object.keys(data)[4]];
				  var aData5 = data[Object.keys(data)[5]];
				  var aData6 = data[Object.keys(data)[6]];
				  var aData7 = data[Object.keys(data)[7]];
				 
				 $('#id').val(aData1);
				 $('#no_kk').val(aData2);
				 $('#kelurahan').val(aData7).change();

				 $('#alamat').val(aData4);
				 $('#latitude').val(aData5);
				 $('#longitude').val(aData6);
				 
				 t_daftar_anggota_kk.api().ajax.url(Dica_app.getPath() + "ktKeluarga/Kt_keluarga/select_anggota_kk?kk=" + aData2).load();
				 
		 });
		 
		 $('#t_daftar_kk tbody').on('click', '.del', function () {
				   var table = $('#t_daftar_kk').DataTable();
				   var data = table.row($(this).closest('tr')).data();
				   $('#code').val(data[Object.keys(data)[1]]);
				  
				  var aData1 = data[Object.keys(data)[1]];
				 
				  
				  bootbox.prompt("Masukan Password untuk menghapus data", function(result){ 
						if (result == 123) 
						{
							ajax = Dica_app.ajax_func(Dica_app.getPath() + "ktKeluarga/Kt_keluarga/save",
										"id=" + aData1 + "&delete=1"
										,"json","POST");
										
								ajax.success(function (res) { 
									if (res.status == 'success'){
										
										bootbox.alert(res.message);    
										setTimeout(function(){ window.open(Dica_app.getPath() + "ktKeluarga/Kt_keluarga", "_self"); }, 2000);
									}else{
										bootbox.alert(res.message);    
									}
								});
								ajax.error(function (xhr, ajaxOptions, thrownError) {
									bootbox.alert(thrownError, xhr.status);
								});	
						}
						else
						{
							alert('Anda tidak memiliki kemampuan untuk menghapus data');
						}
					
					});
		 });
		 
		 
		 
				Dica_app.handle_data_table(
					t_daftar_anggota_kk,
					{
						"order": [
							[1, 'asc']
						],
						"ordering": false,
						"lengthMenu": [
							[10, 15, 20, -1],
							[10, 15, 20, "All"] // change per page values here
						],
						// set the initial value
						"pageLength": 10,
						"pagingType": "full_numbers",
						
						
						"columnDefs": 	[ 
											{	"visible": false, "targets": [1,3]   },
											{  "orderable": false, "targets": 0 }
										],
						"processing": true,
						"serverSide": true,
						"paging": false,
						"searching": false,
						"info": false,
						"ajax": Dica_app.getPath() + "ktKeluarga/Kt_keluarga/select_anggota_kk"
					}
				);
				
				t_daftar_anggota_kk.on('click', '.view', function (e) {
					nRow = $(this).parents('tr')[0];
					aData = t_daftar_anggota_kk.fnGetData(nRow);
					
					 // var aData1 = data[Object.keys(data)[1]];
					  // var aData2 = data[Object.keys(data)[17]];
					  // // alert(aData1);
					  // setTimeout(function(){ window.open(Dica_app.getPath() + "dPenduduk/D_penduduk/v_penduduk?idp=" + aData1 , "_self"); }, 500);
				});
				
		
	};
	
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	var handle_form_modal = function() {
		
		$(document).ready(function()
		{
			
			$("#no_kk").blur(function(){
				$("#hides_dv").show();
				t_daftar_anggota_kk.api().ajax.url(Dica_app.getPath() + "ktKeluarga/Kt_keluarga/select_anggota_kk?kk=" + $('#no_kk').val()).load();
				
			});
			
		});
		
	};
	
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=	

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	
    return {
        //main function to initiate the module
        init: function () {
			
			handle_form();
			handle_form_modal();
			
			handle_datatable();
			// Dica_app.wew();
			Dica_app.validateHandle();
			
        }

    };

}();