var Posisi_keluarga = function () {
	var t_dcuti				= $("#t_dcuti");
	// var t_user_req_ver				= $("#t_user_req_ver");

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	var handle_form = function() {
		$(document).on('click', '#submit_ubah_posisi', function(){
			$("#form_ubah_posisi").data("validator").settings.submitHandler = function (form, validator) {
				if (confirm("Apakah anda yakin untuk menyimpan perubahan data ini ?") == false) {	
						return;
					}
					
					ajax = Dica_app.ajax_func(Dica_app.getPath() + "mPenduduk/Kposisi/save",
										$('#form_ubah_posisi').serialize()
										,"json","POST");
											
					ajax.success(function (res) { 
						if (res.status == 'success'){
							$("#form_ubah_posisi").trigger('reset');
							$("#code").val('');
							$("#posisi").val('');
							bootbox.alert(res.message, function(){ 
								setTimeout(function(){ window.open(Dica_app.getPath() + "mPenduduk/Kposisi", "_self"); }, 100);
							})
							
						}else{
							bootbox.alert(res.message);    
						}	
					});
					ajax.error(function (xhr, ajaxOptions, thrownError) {
						bootbox.alert(thrownError, xhr.status);
					});
				};
		}); 
	};
	
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	var handle_datatable = function() {
		// alert('dicahideto');
		
		$('#t_daftar_kposisi').DataTable( 
			{
				"scrollX": true,
				"columnDefs": [
					{
						"targets": [ 1 ],
						"visible": false,
						"searchable": false,
						
					}
				]
			} );
			
		$('#t_daftar_kposisi tbody').on('click', '.edit', function () {
				   var table = $('#t_daftar_kposisi').DataTable();
				   var data = table.row($(this).closest('tr')).data();
				   $('#code').val(data[Object.keys(data)[1]]);
				  
				  var aData1 = data[Object.keys(data)[1]];
				  var aData2 = data[Object.keys(data)[2]];
				  // alert(aData1);
				  
				  $('#id').val(aData1);
				  $('#posisi').val(aData2);
					$('#mdl').modal('show');
		 });
		 
		 $('#t_daftar_kposisi tbody').on('click', '.del', function () {
				   var table = $('#t_daftar_kposisi').DataTable();
				   var data = table.row($(this).closest('tr')).data();
				   $('#code').val(data[Object.keys(data)[1]]);
				  
				  var aData1 = data[Object.keys(data)[1]];
				  // alert(aData1);
				  
				  bootbox.prompt("Masukan Password untuk menghapus data", function(result){ 
						if (result == 123) 
						{
							ajax = Dica_app.ajax_func(Dica_app.getPath() + "mPenduduk/Kposisi/save",
										"id=" + aData1 + "&delete=1&ia=0"
										,"json","POST");
										
								ajax.success(function (res) { 
									if (res.status == 'success'){
										
										bootbox.alert(res.message);    
										setTimeout(function(){ window.open(Dica_app.getPath() + "mPenduduk/Kposisi", "_self"); }, 2000);
									}else{
										bootbox.alert(res.message);    
									}
								});
								ajax.error(function (xhr, ajaxOptions, thrownError) {
									bootbox.alert(thrownError, xhr.status);
								});	
						}
						else
						{
							alert('Anda tidak memiliki kemampuan untuk menghapus data');
						}
					
					});
		 });
	};

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=	
	var handle_modal = function() {
		$(document).ready(function()
		{
			$("#tambah_posisi").click(function(){
				$('#mdl').modal('show');
			});
		});
	};
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	
    return {
        //main function to initiate the module
        init: function () {
			
			handle_form();
			handle_datatable();
			handle_modal();
			// Dica_app.wew();
			Dica_app.validateHandle();
			
        }

    };

}();