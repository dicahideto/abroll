var T_penduduk = function () {
	var t_dcuti				= $("#t_dcuti");
	// var t_user_req_ver				= $("#t_user_req_ver");

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	var handle_form = function() {
		$(document).on('click', '#submit_tambah_data_penduduk', function(){
			$("#form_tambah_data_penduduk").data("validator").settings.submitHandler = function (form, validator) {
				if (confirm("Apakah anda yakin untuk menyimpan data ini ?") == false) {	
						return;
					}
					
					ajax = Dica_app.ajax_func(Dica_app.getPath() + "dPenduduk/D_penduduk/save",
										$('#form_tambah_data_penduduk').serialize()
										,"json","POST");
											
					ajax.success(function (res) { 
						if (res.status == 'success'){
							$("#form_tambah_data_penduduk").trigger('reset');
							$("#code").val('');
						
							bootbox.alert(res.message, function(){ 
								setTimeout(function(){ window.open(Dica_app.getPath() + "dPenduduk/D_penduduk", "_self"); }, 100);
							})
							
						}else{
							bootbox.alert(res.message);    
						}	
					});
					ajax.error(function (xhr, ajaxOptions, thrownError) {
						bootbox.alert(thrownError, xhr.status);
					});
				};
		}); 
	};
	
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	var handle_datatable = function() {
		
	};

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	
    return {
        //main function to initiate the module
        init: function () {
			
			handle_form();
			handle_datatable();
			// Dica_app.wew();
			Dica_app.validateHandle();
			
        }

    };

}();