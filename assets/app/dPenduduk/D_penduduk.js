var D_penduduk = function () {
	var t_dcuti				= $("#t_dcuti");
	// var t_user_req_ver				= $("#t_user_req_ver");

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	var handle_form_jabatan = function() {
		
	};
	
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	var handle_datatable = function() {
		// alert('dicahideto');
		
		$('#t_daftar_penduduk').DataTable( 
			{
				"scrollX": true,
				"columnDefs": [
					{
						"targets": [ 1 ],
						"visible": false,
						"searchable": false,
						
					}
				]
			} );
			
		$('#t_daftar_penduduk tbody').on('click', '.view', function () {
				   var table = $('#t_daftar_penduduk').DataTable();
				   var data = table.row($(this).closest('tr')).data();
				   $('#code').val(data[Object.keys(data)[1]]);
				  
				  var aData1 = data[Object.keys(data)[1]];
				  var aData2 = data[Object.keys(data)[17]];
				  // alert(aData1);
				  setTimeout(function(){ window.open(Dica_app.getPath() + "dPenduduk/D_penduduk/v_penduduk?idp=" + aData1 , "_self"); }, 500);
			 });
		
		$('#t_daftar_penduduk tbody').on('click', '.edit', function () {
				   var table = $('#t_daftar_penduduk').DataTable();
				   var data = table.row($(this).closest('tr')).data();
				   $('#code').val(data[Object.keys(data)[1]]);
				  
				  var aData1 = data[Object.keys(data)[1]];
				  var aData2 = data[Object.keys(data)[17]];
				  // alert(aData1);
				  
				  setTimeout(function(){ window.open(Dica_app.getPath() + "dPenduduk/D_penduduk/et_penduduk?idp=" + aData1 , "_self"); }, 500);
		 });
		 
		 $('#t_daftar_penduduk tbody').on('click', '.del', function () {
				   var table = $('#t_daftar_penduduk').DataTable();
				   var data = table.row($(this).closest('tr')).data();
				   $('#code').val(data[Object.keys(data)[1]]);
				  
				  var aData1 = data[Object.keys(data)[1]];
				  var aData2 = data[Object.keys(data)[17]];
				  // alert(aData1);
				  
				  bootbox.prompt("Masukan Password untuk menghapus data", function(result){ 
						if (result == 123) 
						{
							ajax = Dica_app.ajax_func(Dica_app.getPath() + "dPenduduk/D_penduduk/save",
										"id=" + aData1 + "&delete=1"
										,"json","POST");
										
								ajax.success(function (res) { 
									if (res.status == 'success'){
										
										bootbox.alert(res.message);    
										setTimeout(function(){ window.open(Dica_app.getPath() + "dPenduduk/D_penduduk", "_self"); }, 2000);
									}else{
										bootbox.alert(res.message);    
									}
								});
								ajax.error(function (xhr, ajaxOptions, thrownError) {
									bootbox.alert(thrownError, xhr.status);
								});	
						}
						else
						{
							alert('Anda tidak memiliki kemampuan untuk menghapus data');
						}
					
					});
		 });
	};
	
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	var handle_form = function() {
		
	};
	
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=	

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	
    return {
        //main function to initiate the module
        init: function () {
			
			handle_form();
			handle_form_jabatan();
			handle_datatable();
			// Dica_app.wew();
			Dica_app.validateHandle();
			
        }

    };

}();