/**
Core script to handle the entire theme and core functions
**/
var Dica_app = function() {
	
    // IE mode
    var isRTL = false;
    var isIE8 = false;
    var isIE9 = false;
    var isIE10 = false;

	var Path = window.location.origin + '/SIDESA/';
	
    var assetsPath = Path + 'assets/';
	
    return {
			
		init: function() {
			
		},	
		wew: function() {
			alert("sdfasdfsdf");
		},
		
		getPath: function() {
            return Path;
        },

        setPath: function(path) {
            Path = path;
        },
		
        getAssetsPath: function() {
            return assetsPath;
        },
		
		select2ajax : function (select_name, placeholder, back, front, table, url, param, value){
			return $(select_name).select2({
				placeholder: placeholder,
				allowClear: true,
				ajax: {
					method: "GET",
					url: url,
					dataType: 'json',
					quietMillis: 100,
					data: function (term, page) {
						return {
							term		: term, //search term
							page_limit	: 10, // page size
							field_back	: back, //field id
							field_front	: front, //field name
							table		: table,
							param		: param,
							value		: value
						};
					},
					results: function (data, page) {
						return { results: data.results };
					}

				},
				initSelection: function(element, callback) {
					return $.getJSON(url + "?id=" + (element.val()) + "&field_back=" + back + "&field_front=" + front + "&table=" + table + "&value=" + value, null, function(data) {

							return callback(data);

					});
				}
				
			});
		},
		
        setAssetsPath: function(path) {
            assetsPath = path;
        },

        setGlobalImgPath: function(path) {
            globalImgPath = assetsPath + path;
        },

        getGlobalImgPath: function() {
            return globalImgPath;
        },
		handle_data_table : function(data_table,data) {
			return  data_table.dataTable(
						data
					);
		},
		
		handle_Link : function(url) {
			handleAjaxLink(url);
		},
		attrDefault : function($el, data_var, default_val) {

			if(typeof $el.data(data_var) != 'undefined')
			{
				return $el.data(data_var);
			}
			return default_val;
		},

		ajax_func : function(url,data,dataType,method) {
			return $.ajax({
				url: url,
				method: method,
				async: true,
				dataType: dataType,
				data: data,
				cache: false
			}); 
		},
		
		iCheckHandle: function(check){
			check.iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue'
			});
		},
		
		validateHandle: function () {
            $("form.validate").each(function (i, el) {
                var $this = $(el),
					opts = {
					    rules: {},
					    messages: {},
					    errorElement: 'span', //default input error message container
					    errorClass: 'help-block', // default input error message class
					    focusInvalid: false, // do not focus the last invalid input
					    ignore: "",
					    highlight: function (element) {
					        $(element).closest('.form-rror').addClass('has-error');
					    },
					    unhighlight: function (element) {
					        $(element).closest('.form-rror').removeClass('has-error');
					    },
					    errorPlacement: function (error, element) {
					        if (element.closest('.has-switch').length) {
					            error.insertAfter(element.closest('.input-icon'));
					        }
					        else
					            if (element.parent('.checkbox, .radio').length || element.parent('.input-group').length) {
					                error.insertAfter(element.parent());
					            }
					            else {
					                error.insertAfter(element);
					            }
					    }
					},
					$fields = $this.find('[data-validate]');


                $fields.each(function (j, el2) {
                    var $field = $(el2),
						name = $field.attr('name'),
						validate = attrDefault($field, 'validate', '').toString(),
						_validate = validate.split(',');

                    for (var k in _validate) {
                        var rule = _validate[k],
							params,
							message;

                        if (typeof opts['rules'][name] == 'undefined') {
                            opts['rules'][name] = {};
                            opts['messages'][name] = {};
                        }

                        if ($.inArray(rule, ['required', 'url', 'email', 'number', 'date', 'creditcard']) != -1) {
                            opts['rules'][name][rule] = true;

                            message = $field.data('message-' + rule);

                            if (message) {
                                opts['messages'][name][rule] = message;
                            }
                        }
                            // Parameter Value (#1 parameter)
                        else
                            if (params = rule.match(/(\w+)\[(.*?)\]/i)) {
                                if ($.inArray(params[1], ['min', 'max', 'minlength', 'maxlength', 'equalTo']) != -1) {
                                    opts['rules'][name][params[1]] = params[2];

                                    message = $field.data('message-' + params[1]);

                                    if (message) {
                                        opts['messages'][name][params[1]] = message;
                                    }
                                }
                            }
                    }
                });

                $this.validate(opts);
            });
        }
		
    };

}();
function attrDefault($el, data_var, default_val) {
    if (typeof $el.data(data_var) != 'undefined') {
        return $el.data(data_var);
    }

    return default_val;
}