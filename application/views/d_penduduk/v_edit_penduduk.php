<?php 
$id_penduduk = $nik = $no_kk = $tempatlahir = $tanggallahir = $kelamin = $posisi_dikeluarga = $Agama = $Pendidikan_terakhir = $profesi = $nama_ibu = $Nama_lengkap = '';

if(!empty($list_data))
{
	$id_penduduk      			  = $list_data->id_penduduk;
	$nik      				  = $list_data->nik;
	$no_kk      			  = $list_data->no_kk;
	$tempatlahir      			  = $list_data->tempatlahir;
	
	$rtanggallahir    	  = $list_data->tanggallahir;
	$tanggallahir		  = ($rtanggallahir == "" || $rtanggallahir == NULL ? NULL : date('d M Y', strtotime(str_replace("-","/",$rtanggallahir))));
	
	$kelamin      				  = $list_data->kelamin;
	$posisi_dikeluarga      				  = $list_data->posisi_dikeluarga;
	$Agama      				  = $list_data->Agama;
	$Pendidikan_terakhir      				  = $list_data->Pendidikan_terakhir;
	$profesi      				  = $list_data->profesi;
	$nama_ibu      				  = $list_data->nama_ibu;
	$Nama_lengkap      				  = $list_data->Nama_lengkap;
	
}

?>

<h3 class="page-title">
Data Penduduk <small>Tambah Data Penduduk</small>
</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="index.html">Home</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Kependudukan</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Data Penduduk</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Tambah Data Penduduk</a>
		</li>
	</ul>
	<div class="page-toolbar">
		
	</div>
</div>

<div class="tab-pane " id="tab_2">
	<div class="portlet">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-reorder"></i>Form Data Penduduk
			</div>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="javascript:;" class="reload"></a>
				<a href="javascript:;" class="remove"></a>
			</div>
		</div>
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			
			<form id="form_tambah_data_penduduk" data-parsley-validate class="form-horizontal form-label-left validate" method="post" action="javascript:;">
				
				<input type="hidden" name="id" value="<?php echo $id_penduduk;?>">
				
				<div class="form-body">
					<h3 class="form-section">No. Identitas</h3>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="control-label col-md-3">NIK</label>
								<div class="col-md-9">
									<input type="text" name="nik" id="nik" class="form-control" placeholder="" required value="<?php echo $nik;?>">
									
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="control-label col-md-3">No. Kartu Keluarga</label>
								<div class="col-md-9">
									<input type="text" name="no_kk" id="no_kk" class="form-control" placeholder="" required value="<?php echo $no_kk;?>">
									
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					
					<h3 class="form-section">Identitas Pelengkap</h3>
					<!--/row-->
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="control-label col-md-3">Nama Lengkap</label>
								<div class="col-md-9">
									<input type="text" name="name" id="name" class="form-control" placeholder="" required value="<?php echo $Nama_lengkap;?>">
									
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-3">Jenis Kelamin</label>
								<div class="col-md-9">
									<div class="radio-list">
											<label class="radio-inline">
											<input type="radio" name="jenis_kelamin" id="jenis_kelamin1" value="Lk" <?php if (isset($kelamin) && $kelamin=="Lk") echo "checked";?>> Laki-Laki </label>
											<label class="radio-inline">
											<input type="radio" name="jenis_kelamin" id="jenis_kelamin2" value="Pr" <?php if (isset($kelamin) && $kelamin=="Pr") echo "checked";?>> Perempuan </label>
											
									</div>
								</div>
								
							</div>
						</div>
						<!--/span-->
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group form-rror">
							<label class="control-label col-md-3">Tempat/ Tanggal Lahir</label>
							<div class="col-md-5">
									<input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" placeholder="" required value="<?php echo $tempatlahir;?>">
									
								</div>
							<div class="col-md-4">
								<div class="input-group input-medium ">
									<input type="text" name="tanggal_lahir" id="tanggal_lahir" class="form-control date date-picker" readonly data-date-format="dd MM yyyy" required value="<?php echo $tanggallahir;?>">
									<span class="input-group-btn">
										<button class="btn default" type="button">
											<i class="fa fa-calendar"></i>
										</button>
									</span>
								</div>
							</div>
						</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="control-label col-md-3">Agama</label>
								<div class="col-md-5">
									<div class="md-group-select">
										<select name="agama" id="agama" class="form-control input-xmedium select2me" data-placeholder="" required>
												<option value=""></option>
												<?php
													if ($Agama != NULL || $Agama != ""){
															foreach($select_agama as $ro):
																if($ro->agama == $Agama):
																	echo "<option value=\"$ro->agama\" selected>$ro->agama</option>";
																else:
																	echo "<option value=\"$ro->agama\">$ro->agama </option>";
																endif;
															endforeach;
													}
													else
													{
														if(!empty($select_agama)){
															foreach($select_agama as $rows_supp):
														?>
														   <option value="<?php echo $rows_supp->agama;?>"><?php echo $rows_supp->agama;?></option>
													   <?php
														endforeach;
														}
													}
												?>
										</select>
									</div>
								
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="control-label col-md-3">Profesi</label>
								<div class="col-md-9">
									<input type="text" name="profesi" id="profesi" class="form-control" placeholder="" required value="<?php echo $id_penduduk;?>">
									
								</div>
							</div>
								
						</div>
					
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="control-label col-md-3">Pendidikan Terakhir</label>
								<div class="col-md-6">
									<div class="md-group-select">
										<select name="pendidikan" id="pendidikan" class="form-control input-xmedium select2me" data-placeholder="" required>
												<option value=""></option>
												<?php
													if ($Pendidikan_terakhir != NULL || $Pendidikan_terakhir != ""){
															foreach($select_pendidikan as $ro):
																if($ro->desc == $Pendidikan_terakhir):
																	echo "<option value=\"$ro->desc\" selected>$ro->desc</option>";
																else:
																	echo "<option value=\"$ro->desc\">$ro->desc </option>";
																endif;
															endforeach;
													}
													else
													{
														if(!empty($select_pendidikan)){
															foreach($select_pendidikan as $rows_supp):
														?>
														   <option value="<?php echo $rows_supp->desc;?>"><?php echo $rows_supp->desc;?></option>
													   <?php
														endforeach;
														}
													}
												?>
										</select>
									</div>
								
								</div>
								
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="control-label col-md-3">Posisi Dikeluarga</label>
								<div class="col-md-6">
									<div class="md-group-select">
										<select name="posisi_keluarga" id="posisi_keluarga" class="form-control input-xmedium select2me" data-placeholder="" required value="<?php echo $id_penduduk;?>">
												<option value=""></option>
												<?php
													if ($posisi_dikeluarga != NULL || $posisi_dikeluarga != ""){
															foreach($select_posisi as $ro):
																if($ro->desc == $posisi_dikeluarga):
																	echo "<option value=\"$ro->desc\" selected>$ro->desc</option>";
																else:
																	echo "<option value=\"$ro->desc\">$ro->desc </option>";
																endif;
															endforeach;
													}
													else
													{
														if(!empty($select_posisi)){
															foreach($select_posisi as $rows_supp):
														?>
														   <option value="<?php echo $rows_supp->desc;?>"><?php echo $rows_supp->desc;?></option>
													   <?php
														endforeach;
														}
													}
												?>
										</select>
									</div>
								
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="control-label col-md-3">Nama Ibu Kandung</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="ibu_kandung" id="ibu_kandung" placeholder="" required value="<?php echo $nama_ibu;?>">
									
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
				</div>
				
				<br />
				<br />
				<div class="form-actions">
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" id="submit_tambah_data_penduduk" class="btn btn-success">Simpan</button>
									<button type="button" class="btn btn-default" onClick="javascript:history.back()">Kembali</button>
								</div>
							</div>
						</div>
						<div class="col-md-6">
						</div>
					</div>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>assets/app/dPenduduk/T_penduduk.js"></script>
<script type="text/javascript">
	T_penduduk.init();
</script>	