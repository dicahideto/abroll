<?php 
$id_penduduk = $nik = $no_kk = $tempatlahir = $tanggallahir = $kelamin = $posisi_dikeluarga = $Agama = $Pendidikan_terakhir = $profesi = $nama_ibu = $Nama_lengkap = '';

if(!empty($list_data))
{
	$id_penduduk      			  = $list_data->id_penduduk;
	$nik      				  = $list_data->nik;
	$no_kk      			  = $list_data->no_kk;
	$tempatlahir      			  = $list_data->tempatlahir;
	
	$rtanggallahir    	  = $list_data->tanggallahir;
	$tanggallahir		  = ($rtanggallahir == "" || $rtanggallahir == NULL ? NULL : date('d M Y', strtotime(str_replace("-","/",$rtanggallahir))));
	
	$kelamin      				  = $list_data->kelamin;
	$posisi_dikeluarga      				  = $list_data->posisi_dikeluarga;
	$Agama      				  = $list_data->Agama;
	$Pendidikan_terakhir      				  = $list_data->Pendidikan_terakhir;
	$profesi      				  = $list_data->profesi;
	$nama_ibu      				  = $list_data->nama_ibu;
	$Nama_lengkap      				  = $list_data->Nama_lengkap;
	
}

?>

<h3 class="page-title">
Data Penduduk <small></small>
</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="index.html">Home</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Kependudukan</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Data Penduduk</a>
		</li>
	</ul>
	<div class="page-toolbar">
		
	</div>
</div>

<div class="tab-pane " id="tab_2">
	<div class="portlet">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-reorder"></i>Data Penduduk
			</div>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="#portlet-config" data-toggle="modal" class="config"></a>
				<a href="javascript:;" class="reload"></a>
				<a href="javascript:;" class="remove"></a>
			</div>
		</div>
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
				<div class="portlet-body form">
					<!-- BEGIN FORM-->
					<form class="form-horizontal" role="form">
						<div class="form-body">
							
							<h3 class="form-section">No. Identitas</h3>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">NIK:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?php echo $nik;?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3"></label>
										<div class="col-md-9">
											<p class="form-control-static">
												 
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">No. Kartu Keluarga:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?php echo $no_kk;?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3"></label>
										<div class="col-md-9">
											<p class="form-control-static">
												
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->
						
							<!--/row-->
							<h3 class="form-section">Identitas Tambahan</h3>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Nama Lengkap:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												  <?php echo $Nama_lengkap;?>
											</p>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Jenis Kelamin:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												  <?php echo $kelamin;?>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Tempat, Tanggal Lahir:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												  <?php echo $tempatlahir;?>, <?php echo $tanggallahir;?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Agama:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												  <?php echo $Agama;?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Profesi:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												  <?php echo $profesi;?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Pendidikan Terakhir:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												  <?php echo $Pendidikan_terakhir;?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Posisi Dikeluarga:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												  <?php echo $posisi_dikeluarga;?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Nama Ibu Kandung:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												  <?php echo $nama_ibu;?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											
											<button type="button" class="btn btn-default" onClick="javascript:history.back()">Kembali</button>
										</div>
									</div>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
					</form>
					<!-- END FORM-->
				</div>
				
				
			<!-- END FORM-->
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>assets/app/source/Form.js"></script>
<script type="text/javascript">
	Form.init();
</script>	