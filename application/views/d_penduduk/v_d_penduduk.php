

<h3 class="page-title">
 Data Penduduk <small></small>
</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="index.html">Home</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Kependudukan</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Data Penduduk</a>
		</li>
	</ul>
	<div class="page-toolbar">
		
	</div>
</div>

<div class="row">
			<div class="col-md-12">
				
				<!-- BEGIN SAMPLE TABLE PORTLET-->
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-cogs"></i>Daftar Penduduk
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
							<a href="#portlet-config" data-toggle="modal" class="config"></a>
							<a href="javascript:;" class="reload"></a>
							<a href="javascript:;" class="remove"></a>
						</div>
					</div>
					<div class="portlet-body flip-scroll">
					
					<br />
				
					
					<a href="<?php echo base_url();?>dPenduduk/D_penduduk/t_penduduk" class="btn btn-sm btn-info"><i class="fa fa-plus"> </i> Tambah Data Penduduk</a>
					
					<br />
					<br />
					
						<table class="table table-bordered table-striped table-condensed flip-content  nowrap cell-border" id="t_daftar_penduduk" width="100%">
						<thead class="flip-content">
						<tr>
							<th width="5px">
								 No.
							</th>
							<th>
								 Id Penduduk
							</th>
							<th>
								 NIK
							</th>
							<th>
								 No KK
							</th>
							<th class="numeric">
								 Nama Penduduk
							</th>
							<th class="numeric">
								 Posisi di Keluarga
							</th>
							<th class="numeric">
								 Aksi
							</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						$total = 0;
						foreach ($table3 as $rows) :?>
						  <tr class="item_row">
								<td><?php echo ++$total; ?></td>
								<td> <?php echo $rows['id_penduduk']; ?></td>
								<td> <?php echo $rows['nik']; ?></td>
								<td> <?php echo $rows['no_kk']; ?></td>
								<td> <?php echo $rows['Nama_lengkap']; ?></td>
								<td> <?php echo $rows['posisi_dikeluarga']; ?></td>
								
								<td>
									<button type="button" class="view btn btn-circle btn-xs btn-default">Lihat</button>
									<button type="button" class="edit btn btn-circle btn-xs btn-dark">Ubah</button>
									<button type="button" class="del btn btn-circle btn-xs btn-danger">Hapus</button>
								</td>
							</tr>
						 <?php endforeach;?>
						</tbody>
						</table>
					</div>
				</div>
				<!-- END SAMPLE TABLE PORTLET-->
			
			</div>
		</div>
		
<script src="<?php echo base_url(); ?>assets/app/dPenduduk/D_penduduk.js"></script>
<script type="text/javascript">
	D_penduduk.init();
</script>		