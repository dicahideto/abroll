<h3 class="page-title">
 Posisi dalam Keluarga <small></small>
</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="index.html">Home</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Master Kependudukan</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Posisi dalam Keluarga</a>
		</li>
	</ul>
	<div class="page-toolbar">
		<div class="btn-group pull-right">
			<button type="button" class="btn btn-fit-height default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
			Actions <i class="fa fa-angle-down"></i>
			</button>
			<ul class="dropdown-menu pull-right" role="menu">
				<li>
					<a href="#">Action</a>
				</li>
				<li>
					<a href="#">Another action</a>
				</li>
				<li>
					<a href="#">Something else here</a>
				</li>
				<li class="divider">
				</li>
				<li>
					<a href="#">Separated link</a>
				</li>
			</ul>
		</div>
	</div>
</div>

<div class="row">
			<div class="col-md-12">
				
				<!-- BEGIN SAMPLE TABLE PORTLET-->
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-cogs"></i>Daftar Posisi dalam Keluarga
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
							<a href="#portlet-config" data-toggle="modal" class="config"></a>
							<a href="javascript:;" class="reload"></a>
							<a href="javascript:;" class="remove"></a>
						</div>
					</div>
					<div class="portlet-body flip-scroll">
					
					<br />
				
					
					<a href="javascript:;" id="tambah_posisi" class="btn btn-sm btn-info"><i class="fa fa-plus"> </i> Tambah Master Pendidikan</a>
					
					<br />
					<br />
					
					<table class="table table-bordered table-striped table-condensed flip-content  nowrap cell-border" id="t_daftar_kposisi" width="100%">
						<thead class="flip-content">
						<tr>
							<th width="5px">
								 No.
							</th>
							<th>
								 Id
							</th>
							<th width="70%">
								 Posisi dalam Keluarga
							</th>
							<th class="numeric">
								 Aksi
							</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						$total = 0;
						foreach ($table3 as $rows) :?>
						  <tr class="item_row">
								<td><?php echo ++$total; ?></td>
								<td> <?php echo $rows['id']; ?></td>
								<td> <?php echo $rows['desc']; ?></td>
								<td>
									<button type="button" class="edit btn btn-circle btn-xs btn-dark">Ubah</button>
									<button type="button" class="del btn btn-circle btn-xs btn-danger">Hapus</button>
								</td>
							</tr>
						 <?php endforeach;?>
						</tbody>
						</table>
					</div>
				</div>
				<!-- END SAMPLE TABLE PORTLET-->
			
			</div>
		</div>

		
<div class="modal fade" id="mdl" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog modal-wide">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Tambah/Update Data</h4>
			</div>
			<div class="modal-body">
					<form id="form_ubah_posisi" data-parsley-validate class="form-horizontal form-label-left validate" method="post" action="javascript:;">
						
						<input type="hidden" id="id" name="id">
						<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-2" for="id">Posisi dalam Keluarga</label>
										<div class="col-md-5">
											<input type="text" id="posisi" name="posisi" class="form-control" required="required" >
										</div>
									</div>
								</div>
						</div> <br />
						
			</div>
			<div class="modal-footer">
				<button type="submit" id="submit_ubah_posisi" class="btn btn-info">Simpan Perubahan</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>


<script src="<?php echo base_url(); ?>assets/app/mPenduduk/Posisi_keluarga.js"></script>
<script type="text/javascript">
	Posisi_keluarga.init();
</script>		