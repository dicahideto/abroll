<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<!-- DOC: for circle icon style menu apply page-sidebar-menu-circle-icons class right after sidebar-toggler-wrapper -->
		<ul class="page-sidebar-menu">
			<li class="sidebar-toggler-wrapper">
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<div class="sidebar-toggler">
				</div>
				<div class="clearfix">
				</div>
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
			</li>
			<li class="sidebar-search-wrapper">
				<form class="search-form" role="form" action="index.html" method="get">
					<div class="input-icon right">
						<i class="icon-magnifier"></i>
						<input type="text" class="form-control" name="query" placeholder="Search...">
					</div>
				</form>
			</li>
			
		
			<?php
			
						// $this->db->where('menu_id',$m1->code);
						$this->db->where('isParent',1);
						$this->db->where('isActive',1);
						// $this->db->where('m_user_group',$this->session->userdata('sessCodeBidang'));
						$menu2 = $this->db->get('m_menu');
						if($menu2->num_rows() > 0)
						{
							foreach($menu2->result() as $m2)
							{
								if ($m2->have_sm == 0 || $m2->url == NULL)
								{
									echo "
									
									<li>
										<a href='".base_url($m2->url)."' class='nav-link '>
											<i class='".$m2->icon."'></i>
											<span class='title'>".$m2->name."</span>
										</a>
									";	
								}
								else
								{
									echo "
									<li >
										<a href='javascript:;'>
											<i class='".$m2->icon."'></i>
											<span class='title'>".$m2->name."</span>
											<span class='arrow'></span>
										</a>
									";	
								}
								
								// $this->db->where('menu_id',$m1->code);
								$this->db->where('menu_id_child',$m2->code);
								$this->db->where('isParent',2);
								$this->db->where('isActive',1);
								// $this->db->where('m_user_group',$this->session->userdata('sessCodeBidang'));
								$menu3 = $this->db->get('m_menu');
								if($menu3->num_rows() > 0)
								{
									echo "
										<ul class='sub-menu'>
									";
									foreach($menu3->result() as $m3)
									{
										echo "
										
											<li >
											";
											if ($m3->url == '')
											{
												echo "
													<a href='javascript:;' >
														<i class='".$m3->icon."'></i>
														<span class='title'>".$m3->name."</span>
														<span class='arrow'></span>
													</a>
												";
											}
											else
											{
												echo "
													<a href='".base_url($m3->url)."' >
														<i class='".$m3->icon."'></i>
														<span class='title'>".$m3->name."</span>
													</a>
												";
											}	
											echo "
												
													
											";	
												
											// $this->db->where('menu_id',$m2->code);
											$this->db->where('menu_id_child',$m3->code);
											$this->db->where('isParent',3);
											$this->db->where('isActive',1);
											// $this->db->where('m_user_group',$this->session->userdata('sessCodeBidang'));
											$menu4 = $this->db->get('m_menu');
											if($menu4->num_rows() > 0)
											{	
												echo "<ul class='sub-menu'>";
												foreach($menu4->result() as $m4)
												{
													
													echo "
														<li >
															<a href='".base_url($m4->url)."' >".$m4->name."</a>
														</li>
													
													";
												}
												echo "</ul>";
											}
										echo"
											</li>
										
										";
									}
									echo "
										</ul>
									";
								}
								
								echo "	
								</li>
								";
								
							}
						}
					
			?>
			
			
			
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>