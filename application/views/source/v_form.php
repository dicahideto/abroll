<h3 class="page-title">
Responsive Tables <small>responsive table samples</small>
</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="index.html">Home</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Data Tables</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Responsive Tables</a>
		</li>
	</ul>
	<div class="page-toolbar">
		<div class="btn-group pull-right">
			<button type="button" class="btn btn-fit-height default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
			Actions <i class="fa fa-angle-down"></i>
			</button>
			<ul class="dropdown-menu pull-right" role="menu">
				<li>
					<a href="#">Action</a>
				</li>
				<li>
					<a href="#">Another action</a>
				</li>
				<li>
					<a href="#">Something else here</a>
				</li>
				<li class="divider">
				</li>
				<li>
					<a href="#">Separated link</a>
				</li>
			</ul>
		</div>
	</div>
</div>

<div class="tab-pane " id="tab_2">
	<div class="portlet">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-reorder"></i>Form Sample
			</div>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				<a href="#portlet-config" data-toggle="modal" class="config"></a>
				<a href="javascript:;" class="reload"></a>
				<a href="javascript:;" class="remove"></a>
			</div>
		</div>
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<form id="" data-parsley-validate class="form-horizontal form-label-left validate" method="post" action="<?php echo base_url();?>source/Form" enctype="multipart/form-data" />
				<div class="form-body">
					<h3 class="form-section">Person Info</h3>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="control-label col-md-3">First Name</label>
								<div class="col-md-9">
									<input type="text" class="form-control" placeholder="Chee Kin" required>
									<span class="help-block">
									This is inline help </span>
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="control-label col-md-3">Last Name</label>
								<div class="col-md-9">
									<input type="text" class="form-control" placeholder="Lim" >
									<span class="help-block">
									This field has error. </span>
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="control-label col-md-3">Choice</label>
								<div class="col-md-9">
									<div class="md-group-select">
										<select name="sdfads" class="form-control input-xlarge select2me" data-placeholder="Select..." required>
												<option value=""></option>
												<option value="AL">Alabama</option>
												<option value="WY">Wyoming</option>
											</select>
									</div>
									<span class="help-block">
									Select your gender. </span>
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group form-rror">
										<label class="control-label col-md-3">Default Datepicker</label>
										<div class="col-md-3">
											<input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="" required/>
											<span class="help-block">
											Select date </span>
										</div>
								</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group form-rror">
							<label class="control-label col-md-3">Date of Birth</label>
							<div class="col-md-9">
								<div class="input-group input-medium ">
									<input type="text" name="sdafd" class="form-control date date-picker" readonly data-date-format="dd MM yyyy" required>
									<span class="input-group-btn">
										<button class="btn default" type="button">
											<i class="fa fa-calendar"></i>
										</button>
									</span>
								</div>
							</div>
						</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
							<label class="control-label col-md-3">Date of Birth</label>
							<div class="col-md-9">
								<div class="input-group input-medium ">
									<input type="text" class="form-control date date-picker" readonly data-date-format="dd MM yyyy" >
									<span class="input-group-btn">
										<button class="btn default" type="button">
											<i class="fa fa-calendar"></i>
										</button>
									</span>
								</div>
							</div>
						</div>
						</div>
						<!--/span-->
					</div>
					<h3 class="form-section">Address</h3>
					<!--/row-->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-3">Checkbox 1</label>
								<div class="col-md-9">
									<div class="checkbox-list">
										<label>
										<input type="checkbox"> Checkbox 1 </label>
										<label>
										<input type="checkbox"> Checkbox 2 </label>
										<label>
										<input type="checkbox" disabled> Disabled </label>
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-3">Checkbox 2</label>
								<div class="col-md-9">
									<div class="checkbox-list">
										<label class="checkbox-inline">
										<input type="checkbox" id="inlineCheckbox1" value="option1"> Checkbox 1 </label>
										<label class="checkbox-inline">
										<input type="checkbox" id="inlineCheckbox2" value="option2"> Checkbox 2 </label>
										<label class="checkbox-inline">
										<input type="checkbox" id="inlineCheckbox3" value="option3" disabled> Disabled </label>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-3">Checkbox 1</label>
								<div class="col-md-9">
									<div class="radio-list">
											<label>
											<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked> Option 1 </label>
											<label>
											<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2"> Option 2 </label>
											<label>
											<input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled> Disabled </label>
										</div>
								</div>
								
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label col-md-3">Checkbox 2</label>
								<div class="col-md-9">
									<div class="radio-list">
											<label class="radio-inline">
											<input type="radio" name="optionsRadios" id="optionsRadios4" value="option1" checked> Option 1 </label>
											<label class="radio-inline">
											<input type="radio" name="optionsRadios" id="optionsRadios5" value="option2"> Option 2 </label>
											<label class="radio-inline">
											<input type="radio" name="optionsRadios" id="optionsRadios6" value="option3" disabled> Disabled </label>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="control-label col-md-3">CKEditor <span class="required">
								* </span>
								</label>
								<div class="col-md-9">
									<textarea class="ckeditor form-control" name="editor2"  rows="6" required></textarea>
									<div id="editor2_error">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="col-md-3 control-label">Textarea</label>
								<div class="col-md-9">
									<textarea class="form-control target_autogrow" rows="3" name="sdfasdf" required></textarea>
								</div>
							</div>
						</div>
					</div>
					
					
					<!--/row-->
					<div class="row">
							<div class="col-md-6">
								<label class="control-label col-md-3">Image Upload #2</label>
										<div class="col-md-9">
											<div class="fileinput fileinput-new" data-provides="fileinput">
												<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
													<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
												</div>
												<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
												</div>
												<div>
													<span class="btn btn-default btn-file">
													<span class="fileinput-new">
													Select image </span>
													<span class="fileinput-exists">
													Change </span>
													<input type="file" name="...">
													</span>
													<a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">
													Remove </a>
												</div>
											</div>
											<div class="clearfix margin-top-10">
												<span class="label label-danger">
												NOTE! </span>
												Image preview only works in IE10+, FF3.6+, Safari6.0+, Chrome6.0+ and Opera11.1+. In older browsers the filename is shown instead.
											</div>
										</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								
							</div>
							<!--/span-->
					</div>
					<!--/row-->
				</div>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn btn-success">Submit</button>
									<button type="button" class="btn btn-default">Cancel</button>
								</div>
							</div>
						</div>
						<div class="col-md-6">
						</div>
					</div>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>assets/app/source/Form.js"></script>
<script type="text/javascript">
	Form.init();
</script>	