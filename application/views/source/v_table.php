<h3 class="page-title">
Responsive Tables <small>responsive table samples</small>
</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="index.html">Home</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Data Tables</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Responsive Tables</a>
		</li>
	</ul>
	<div class="page-toolbar">
		<div class="btn-group pull-right">
			<button type="button" class="btn btn-fit-height default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
			Actions <i class="fa fa-angle-down"></i>
			</button>
			<ul class="dropdown-menu pull-right" role="menu">
				<li>
					<a href="#">Action</a>
				</li>
				<li>
					<a href="#">Another action</a>
				</li>
				<li>
					<a href="#">Something else here</a>
				</li>
				<li class="divider">
				</li>
				<li>
					<a href="#">Separated link</a>
				</li>
			</ul>
		</div>
	</div>
</div>

<div class="row">
			<div class="col-md-12">
				<div class="note note-success">
					<p>
						 Please try to re-size your browser window in order to see the tables in responsive mode.
					</p>
				</div>
				<!-- BEGIN SAMPLE TABLE PORTLET-->
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-cogs"></i>Responsive Flip Scroll Tables
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
							<a href="#portlet-config" data-toggle="modal" class="config"></a>
							<a href="javascript:;" class="reload"></a>
							<a href="javascript:;" class="remove"></a>
						</div>
					</div>
					<div class="portlet-body flip-scroll">
						<table class="table table-bordered table-striped table-condensed flip-content  nowrap cell-border" id="t_table" width="100%">
						<thead class="flip-content">
						<tr>
							<th width="5px">
								 Code
							</th>
							<th>
								 Username
							</th>
							<th class="numeric">
								 Password
							</th>
							<th class="numeric">
								 Name
							</th>
							<th class="numeric">
								 Aksi
							</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						$total = 0;
						foreach ($table3 as $rows) :?>
						  <tr class="item_row">
								<td><?php echo ++$total; ?></td>
								<td> <?php echo $rows['username']; ?></td>
								<td> <?php echo $rows['password']; ?></td>
								<td> <?php echo $rows['name']; ?></td>
								
								<td>
									<button type="button" class="view btn btn-circle btn-xs btn-default">View</button>
									<button type="button" class="edit btn btn-circle btn-xs btn-dark">Ubah</button>
								</td>
							</tr>
						 <?php endforeach;?>
						</tbody>
						</table>
					</div>
				</div>
				<!-- END SAMPLE TABLE PORTLET-->
			
			</div>
		</div>
		
<script src="<?php echo base_url(); ?>assets/app/source/Table.js"></script>
<script type="text/javascript">
	Table.init();
</script>		