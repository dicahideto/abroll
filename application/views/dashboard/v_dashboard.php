
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
Dashboard <small>statistics and more</small>
</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="index.html">Home</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Dashboard</a>
		</li>
	</ul>
	<div class="page-toolbar">
		<div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height btn-primary" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
			<i class="icon-calendar"></i>&nbsp; <span class="thin uppercase visible-lg-inline-block"></span>&nbsp; <i class="fa fa-angle-down"></i>
		</div>
	</div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN OVERVIEW STATISTIC BARS-->
<div class="row stats-overview-cont">
	<div class="col-md-2 col-sm-4">
		<div class="stats-overview stat-block">
			<div class="display stat ok huge">
				<span class="line-chart">
				5, 6, 7, 11, 14, 10, 15, 19, 15, 2 </span>
				<div class="percent">
					 +66%
				</div>
			</div>
			<div class="details">
				<div class="title">
					 Users
				</div>
				<div class="numbers">
					 1360
				</div>
			</div>
			<div class="progress">
				<span style="width: 40%;" class="progress-bar progress-bar-info" aria-valuenow="66" aria-valuemin="0" aria-valuemax="100">
				<span class="sr-only">
				66% Complete </span>
				</span>
			</div>
		</div>
	</div>
	<div class="col-md-2 col-sm-4">
		<div class="stats-overview stat-block">
			<div class="display stat good huge">
				<span class="line-chart">
				2,6,8,12, 11, 15, 16, 11, 16, 11, 10, 3, 7, 8, 12, 19 </span>
				<div class="percent">
					 +16%
				</div>
			</div>
			<div class="details">
				<div class="title">
					 Site Visits
				</div>
				<div class="numbers">
					 1800
				</div>
				<div class="progress">
					<span style="width: 16%;" class="progress-bar progress-bar-warning" aria-valuenow="16" aria-valuemin="0" aria-valuemax="100">
					<span class="sr-only">
					16% Complete </span>
					</span>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2 col-sm-4">
		<div class="stats-overview stat-block">
			<div class="display stat bad huge">
				<span class="line-chart">
				2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3 </span>
				<div class="percent">
					 +6%
				</div>
			</div>
			<div class="details">
				<div class="title">
					 Orders
				</div>
				<div class="numbers">
					 509
				</div>
				<div class="progress">
					<span style="width: 16%;" class="progress-bar progress-bar-success" aria-valuenow="16" aria-valuemin="0" aria-valuemax="100">
					<span class="sr-only">
					16% Complete </span>
					</span>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2 col-sm-4">
		<div class="stats-overview stat-block">
			<div class="display stat good huge">
				<span class="bar-chart">
				1,4,9,12, 10, 11, 12, 15, 12, 11, 9, 12, 15, 19, 14, 13, 15 </span>
				<div class="percent">
					 +86%
				</div>
			</div>
			<div class="details">
				<div class="title">
					 Revenue
				</div>
				<div class="numbers">
					 1550
				</div>
				<div class="progress">
					<span style="width: 56%;" class="progress-bar progress-bar-warning" aria-valuenow="56" aria-valuemin="0" aria-valuemax="100">
					<span class="sr-only">
					56% Complete </span>
					</span>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2 col-sm-4">
		<div class="stats-overview stat-block">
			<div class="display stat ok huge">
				<span class="line-chart">
				2,6,8,12, 11, 15, 16, 17, 14, 12, 10, 8, 10, 2, 4, 12, 19 </span>
				<div class="percent">
					 +72%
				</div>
			</div>
			<div class="details">
				<div class="title">
					 Sales
				</div>
				<div class="numbers">
					 9600
				</div>
				<div class="progress">
					<span style="width: 72%;" class="progress-bar progress-bar-danger" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100">
					<span class="sr-only">
					72% Complete </span>
					</span>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-2 col-sm-4">
		<div class="stats-overview stat-block">
			<div class="display stat bad huge">
				<span class="line-chart">
				1,7,9,11, 14, 12, 6, 7, 4, 2, 9, 8, 11, 12, 14, 12, 10 </span>
				<div class="percent">
					 +15%
				</div>
			</div>
			<div class="details">
				<div class="title">
					 Stock
				</div>
				<div class="numbers">
					 2090
				</div>
				<div class="progress">
					<span style="width: 15%;" class="progress-bar progress-bar-success" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100">
					<span class="sr-only">
					15% Complete </span>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END OVERVIEW STATISTIC BARS-->
<div class="clearfix">
</div>
<div class="row">
	<div class="col-md-6 col-sm-12">
		<!-- BEGIN PORTLET-->
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-bar-chart"></i>Tab Monitoring
				</div>
				<div class="actions">
					<div class="btn-group" data-toggle="buttons">
						<label class="btn btn-default btn-sm active">
						<input type="radio" name="options" class="toggle">New </label>
						<label class="btn btn-default btn-sm">
						<input type="radio" name="options" class="toggle">Returning </label>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div id="site_statistics_loading">
					<img src="<?php echo base_url();?>assets/img/loading.gif" alt="loading"/>
				</div>
				<div id="site_statistics_content" class="display-none">
					<div id="site_statistics" class="chart">
					</div>
				</div>
			</div>
		</div>
		<!-- END PORTLET-->
	</div>
	<div class="col-md-6 col-sm-12">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-bar-chart"></i>Tab Monitoring
				</div>
				<div class="tools">
					<a href="" class="collapse"></a>
					<a href="#portlet-config" data-toggle="modal" class="config"></a>
					<a href="" class="reload"></a>
					<a href="" class="remove"></a>
				</div>
			</div>
			<div class="portlet-body">
				<!--BEGIN TABS-->
				
				<div class="tab-content">
					
				
				</div>
				<!--END TABS-->
			</div>
		</div>
	</div>
</div>
<div class="clearfix">
</div>

<div class="clearfix">
</div>

<div class="clearfix">
</div>
<!-- BEGIN OVERVIEW STATISTIC BLOCKS-->
<div class="row">
	<div class="col-md-3 col-sm-6">
		<div class="circle-stat stat-block">
			<div class="visual">
				<input class="knobify" data-width="115" data-thickness=".2" data-skin="tron" data-displayprevious="true" value="+89" data-max="100" data-min="-100"/>
			</div>
			<div class="details">
				<div class="title">
					 Unique Visits <i class="fa fa-caret-up"></i>
				</div>
				<div class="number">
					 10112
				</div>
				<span class="label label-success">
				<i class="fa fa-map-marker"></i> 123 </span>
				<span class="label label-warning">
				<i class="fa fa-comment"></i> 3 </span>
				<span class="label label-info">
				<i class="fa fa-bullhorn"></i> 3 </span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6">
		<div class="circle-stat stat-block">
			<div class="visual">
				<input class="knobify" data-width="115" data-fgcolor="#66EE66" data-thickness=".2" data-skin="tron" data-displayprevious="true" value="+19" data-max="100" data-min="-100"/>
			</div>
			<div class="details">
				<div class="title">
					 New Users <i class="fa fa-caret-up"></i>
				</div>
				<div class="number">
					 987
				</div>
				<span class="label label-danger">
				<i class="fa fa-bullhorn"></i> 567 </span>
				<span class="label label-default">
				<i class="fa fa-plus"></i> 16 </span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6" data-tablet="span6 fix-margin" data-desktop="span3">
		<div class="circle-stat stat-block">
			<div class="visual">
				<input class="knobify" data-width="115" data-fgcolor="#e23e29" data-thickness=".2" data-skin="tron" data-displayprevious="true" value="-12" data-max="100" data-min="-100"/>
			</div>
			<div class="details">
				<div class="title">
					 Downtime <i class="fa fa-caret-down down"></i>
				</div>
				<div class="number">
					 0.01%
				</div>
				<span class="label label-info">
				<i class="fa fa-bookmark-empty"></i> 23 </span>
				<span class="label label-warning">
				<i class="fa fa-check"></i> 31 </span>
				<span class="label label-success">
				<i class="fa fa-briefcase"></i> 39 </span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6">
		<div class="circle-stat stat-block">
			<div class="visual">
				<input class="knobify" data-width="115" data-fgcolor="#fdbb39" data-thickness=".2" data-skin="tron" data-displayprevious="true" value="+58" data-max="100" data-min="-100"/>
			</div>
			<div class="details">
				<div class="title">
					 Profit <i class="fa fa-caret-up"></i>
				</div>
				<div class="number">
					 1120.32$
				</div>
				<span class="label label-success">
				<i class="fa fa-comment"></i> 453 </span>
				<span class="label label-default">
				<i class="fa fa-globe"></i> 123 </span>
			</div>
		</div>
	</div>
</div>
<!-- END OVERVIEW STATISTIC BLOCKS-->
<div class="clearfix">
</div>



<script src="<?php echo base_url(); ?>assets/app/dashboard/Dashboard.js"></script>
<script type="text/javascript">
	Dashboard.init();
</script>		