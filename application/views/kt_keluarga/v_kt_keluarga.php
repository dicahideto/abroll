

<h3 class="page-title">
 Data Penduduk <small></small>
</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="index.html">Home</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Kependudukan</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Data Penduduk</a>
		</li>
	</ul>
	<div class="page-toolbar">
		
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		
		<!-- BEGIN SAMPLE TABLE PORTLET-->
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs"></i>Buat Kartu Keluarga
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a>
					<a href="#portlet-config" data-toggle="modal" class="config"></a>
					<a href="javascript:;" class="reload"></a>
					<a href="javascript:;" class="remove"></a>
				</div>
			</div>
			<div class="portlet-body form">
			
			<br />
			
			<form id="form_kk" data-parsley-validate class="form-horizontal form-label-left validate" method="post" action="javascript:;">
		
				<div class="form-body">
					<h3 class="form-section">No. Identitas</h3>
					
					<input type="hidden" name="id" id="id">
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="control-label col-md-3">No. Kartu Keluarga</label>
								<div class="col-md-7">
									<input type="text" name="no_kk" id="no_kk" class="form-control" placeholder="" required>
									
								</div>
								<div class="col-md-3">
									
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							
						</div>
						<!--/span-->
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="control-label col-md-3">Kelurahan</label>
								<div class="col-md-6">
									<div class="md-group-select">
										<select name="kelurahan" id="kelurahan" class="form-control input-xmedium select2me" data-placeholder="" required>
												<option value=""></option>
												<?php
													
														if(!empty($select_kelurahan)){
															foreach($select_kelurahan as $rows_supp):
														?>
														   <option value="<?php echo $rows_supp->id_kelurahan;?>"><?php echo $rows_supp->nama_kelurahaan;?></option>
													   <?php
														endforeach;
														}
													
												?>
										</select>
									</div>
								
								</div>
								
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="col-md-3 control-label">Alamat</label>
								<div class="col-md-8">
									<textarea class="form-control target_autogrow" rows="3" name="alamat" id="alamat" required></textarea>
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="control-label col-md-3">Latitude</label>
								<div class="col-md-6">
									<input type="number" name="latitude" id="latitude" class="form-control" placeholder="" >
								
								</div>
								
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group form-rror">
								<label class="col-md-3 control-label">Longitude</label>
								<div class="col-md-6">
									<input type="number" name="longitude" id="longitude" class="form-control" placeholder="" >
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					
					
					
						<div id="hides_dv" style="display:show;">
								<h3 class="form-section">Identitas Pelengkap</h3>
								<!--/row-->
								
								<table class="table table-bordered table-striped table-condensed flip-content  " id="t_daftar_anggota_kk" width="100%">
									<thead class="flip-content">
									<tr>
										<th width="5px">
											 No.
										</th>
										<th>
											 Id 
										</th>
										<th >
											 NIK
										</th>
										<th >
											 No KK
										</th>
										<th >
											 Nama
										</th>
										<th >
											 Posisi Dikeluarga
										</th>
										<th >
											 Kelamin
										</th>
										<th >
											 Tempat Lahir
										</th>
										<th >
											 Tanggal Lahir
										</th>
										
									</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
					
					</div>
				</div>
				
				<br />
				<br />
				<div class="form-actions">
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-offset-3 col-md-9" id="actions">
									<button type="submit" id="submit_form_kk" class="btn btn-success">Simpan</button>
									<button type="button" class="btn btn-default" onClick="javascript:history.back()">Kembali</button>
								</div>
							</div>
						</div>
						<div class="col-md-6">
						</div>
					</div>
				</div>
			</form>
			<!-- END FORM-->
			
			
			</div>
		</div>
		<!-- END SAMPLE TABLE PORTLET-->
	
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		
		<!-- BEGIN SAMPLE TABLE PORTLET-->
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs"></i>Daftar Penduduk
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a>
					<a href="#portlet-config" data-toggle="modal" class="config"></a>
					<a href="javascript:;" class="reload"></a>
					<a href="javascript:;" class="remove"></a>
				</div>
			</div>
			<div class="portlet-body flip-scroll">
			
			<br />
		
			
			<a href="<?php echo base_url();?>dPenduduk/D_penduduk/t_penduduk" class="btn btn-sm btn-info"><i class="fa fa-plus"> </i> Tambah Data Penduduk</a>
			
			<br />
			<br />
			
				<table class="table table-bordered table-striped table-condensed flip-content  nowrap cell-border" id="t_daftar_kk" width="100%">
					<thead class="flip-content">
					<tr>
						<th width="5px">
							 No.
						</th>
						<th>
							 Id 
						</th>
						<th>
							 No KK
						</th>
						
						<th >
							 Kelurahan
						</th>
						<th >
							 Alamat
						</th>
						<th >
							 Latitude
						</th>
						<th >
							 Longitude
						</th>
						<th >
							 Id Kelurahan
						</th>
						<th >
							 Aksi
						</th>
					</tr>
					</thead>
					<tbody>
					<?php 
					$total = 0;
					foreach ($table3 as $rows) :?>
						<tr class="item_row">
							<td><?php echo ++$total; ?></td>
							<td> <?php echo $rows['id_kk']; ?></td>
							<td> <?php echo $rows['no_kk']; ?></td>
							<td> <?php echo $rows['nama_kelurahaan']; ?></td>
							
							<td> <?php echo $rows['alamat']; ?></td>
							<td> <?php echo $rows['lantitude']; ?></td>
							<td> <?php echo $rows['Longtitude']; ?></td>
							<td> <?php echo $rows['id_kelurahan']; ?></td>
							
							<td>
								<button type="button" class="view btn btn-circle btn-xs btn-default">Lihat</button>
								<button type="button" class="edit btn btn-circle btn-xs btn-dark">Ubah</button>
								<button type="button" class="del btn btn-circle btn-xs btn-danger">Hapus</button>
							</td>
						</tr>
					 <?php endforeach;?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END SAMPLE TABLE PORTLET-->
	
	</div>
</div>
		
<script src="<?php echo base_url(); ?>assets/app/ktKeluarga/KtKeluarga.js"></script>
<script type="text/javascript">
	KtKeluarga.init();
</script>		