<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kt_keluarga extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->model('ssp_class');
		$this->load->model('model');
		$this->load->library('template');
		$this->simple_login->cek_login();
		
		$this->sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);
    }

	public function index()
	{
		$data['code_prefix'] = '';
		
		$tambahan_cus	= "from 
						(
							select
							kk.*,
							km.nama_kelurahaan
							from
							kartu_keluarga_master kk
							left join 
							kelurahan_master km on (kk.id_kelurahan = km.id_kelurahan)
						) as a order by updated_date desc";	
						
		$query3 = $this->db->query('SELECT * '.$tambahan_cus);
		$data['select_kelurahan'] 	= $this->model->cetak('from kelurahan_master');

		$data['table3'] 		= $query3->result_array();
		
		$this->template->display('kt_keluarga/v_kt_keluarga', $data);
		
	}
	
	public function select_anggota_kk()  
	{
		$no_kk	= $this->input->get('kk');
		
		$link = 100;
		$table = 'penduduk_master';
		$tambahan	= "from 
						(
							select 
							*
							from 
							penduduk_master
						) as a";
						
		$primaryKey = 'id_penduduk';
		$columns = array(
			array( 'db' => 'id_penduduk', 
					'dt' => 0,
					'numberrow' => function( $d, $row ) {
									if ($d == NULL){
										return $row;
									}
									else
									{
										return $row;	
									}
						}),
			array( 'db' => 'id_penduduk', 'dt' => 1 ),
			array( 'db' => 'nik', 
					'dt' => 2,
					'formatter' => function( $d, $row ) {
									if ($d == NULL){
										return '';
									}
									else
									{
										return floatval($d);;	
									}
						}),
			array( 'db' => 'no_kk', 'dt' => 3 ),
			array( 'db' => 'Nama_lengkap', 'dt' => 4 ),
			array( 'db' => 'posisi_dikeluarga', 'dt' => 5 ),
			array( 'db' => 'kelamin', 'dt' => 6),
			array( 'db' => 'tempatlahir', 'dt' => 7 ),
			array( 'db' => 'tanggallahir', 
					'dt' => 8,
					'formatter' => function( $d, $row ) {
									if ($d == NULL){
										return $d;
									}
									else
									{
										return date( 'd M Y' , strtotime($d));	
									}
						}),
			
			
			
			
		);
		
		if ($no_kk == '' || $no_kk == null)
		{
			$filtering_manual	= "no_kk is null ";
		}
		else
		{
			$filtering_manual	= "no_kk = ".$no_kk;
		}
		
		// $filtering_manual	= "";
		
		$columns_tambahan 	= '
								
								
						  ';
		echo json_encode(
			ssp_class::simple_II( $_GET, $this->sql_details, $table, $primaryKey, $columns , $columns_tambahan, $link, $filtering_manual, $tambahan)
		);
	}
	
	
	
	public function save()
	{
		
		$code_jabatan				= $this->session->userdata('sessCodeBidang');
		
		$del						= $this->input->post('delete');
	
		$id							= $this->input->post('id');
		
		$no_kk					= $this->input->post('no_kk');
		$kelurahan				= $this->input->post('kelurahan');
		$alamat					= $this->input->post('alamat');
		$latitude				= $this->input->post('latitude');
		$longitude				= $this->input->post('longitude');
	
		$table						= 'kartu_keluarga_master';
		
		$udah_ada 			= false;
		$resp['edit'] 		= '';
		
		
		if ($del == ''){
			$q_cek	= $this->db->query("SELECT id_kk FROM ".$table." WHERE id_kk = '".$id."'");
			$j_cek	= $q_cek->num_rows();
			if($j_cek == 1) {
				$udah_ada	= true;
			}
			if ($udah_ada)
			{
				$data 		= array('no_kk' => $no_kk, 'id_kelurahan' => $kelurahan, 'alamat' => $alamat, 'lantitude' => $latitude, 'Longtitude' => $longitude);
					
				$type		= "update";
				$message	= 'Berhasil memperbarui Data ';
				$table_id	= "id_kk";
				
				$query				= $this->model->m_iud($type, $data, $table, $table_id , $id, $message);
				$resp['error'] 		= $query['error'];
				$resp['message']	= $query['message'];
				$resp['status']		= $query['status'];
				
				echo json_encode($resp);
				
			}
			else
			{
					$sql		 = "SELECT MAX(id_kk) as tmpNo FROM ".$table;
					$query		 = $this->db->query($sql);
					
					foreach($query->result() as $row){
						$tmpNo	= $row->tmpNo;
						if(is_null($row->tmpNo)){
							$tmpNo	= 1;
						}
						else{
							$tmpNo 	= $tmpNo+1;
						}
					}
			
					$data 		= array('id_kk' => $tmpNo, 'no_kk' => $no_kk, 'id_kelurahan' => $kelurahan, 'alamat' => $alamat, 'lantitude' => $latitude, 'Longtitude' => $longitude);
						
					$type			= "insert";
					$message		= 'Berhasil menyimpan Data ';
					$table_id		= "id_kk";
						
					$querys				= $this->model->m_iud($type, $data, $table, $table_id , $id, $message);
					$resp['error'] 		= $querys['error'];
					$resp['message']	= $querys['message'];
					$resp['status']		= $querys['status'];
						
					echo json_encode($resp);
					
				
				
			}
		}
		else
		{
			$data 		= "";
			$type		= "delete";
			$message	= 'Data berhasil dihapus';
			$table_id	= "id_kk";
			
			$query			= $this->model->m_iud($type, $data, $table, $table_id ,$id, $message);
			$resp['error'] 		= $query['error'];
			$resp['message']	= $query['message'];
			$resp['status']		= $query['status'];
			
			if ($query){
				$sql		= "ALTER TABLE ".$table." AUTO_INCREMENT = 1";
				$this->db->query($sql); 
			}	
			
			echo json_encode($resp);
				
		}
		
		// $resp['error'] 		= '';
		// $resp['message']	= "Data anda gagal diproses, Mohon diulang kembali";
		// $resp['status']		= 'success';
		
		// echo json_encode($resp);
	}
}
