<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agama extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->model('ssp_class');
		$this->load->model('model');
		$this->load->library('template');
		$this->simple_login->cek_login();
		
		$this->sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);
    }

	public function index()
	{
		$data['code_prefix'] = '';
		
		$where = "where isActive in (1)";
		
		$tambahan_cus	= "from 
						(
							select
							*
							from
							agama_master
							
						) as a ";	
						
		$query3 = $this->db->query('SELECT * '.$tambahan_cus." ".$where );

		$data['table3'] = $query3->result_array();
		
		$this->template->display('m_penduduk/v_agama', $data);
		
	}
	
	
	public function save()
	{
		
		$code_jabatan		= $this->session->userdata('sessCodeBidang');
		
		$del				= $this->input->post('delete');
	
		$id					= $this->input->post('id');
		$agama				= $this->input->post('agama');
		$ia					= $this->input->post('ia');
		
		$table				= 'agama_master';
		
		$udah_ada 			= false;
		$resp['edit'] 		= '';
		
		
		if ($del == ''){
			$q_cek	= $this->db->query("SELECT id FROM ".$table." WHERE id = '".$id."'");
			$j_cek	= $q_cek->num_rows();
			if($j_cek == 1) {
				$udah_ada	= true;
			}
			if ($udah_ada)
			{
				$data 		= array('agama' => $agama);
					
				$type		= "update";
				$message	= 'Berhasil memperbarui Data';
				$table_id	= "id";
				
				$query				= $this->model->m_iud($type, $data, $table, $table_id, $id, $message);
				$resp['error'] 		= $query['error'];
				$resp['message']	= $query['message'];
				$resp['status']		= $query['status'];
				
				echo json_encode($resp);
				
			}
			else
			{
				$sql		 = "SELECT MAX(id) as tmpNo FROM ".$table;
				$query		 = $this->db->query($sql);
				
				foreach($query->result() as $row){
					$tmpNo	= $row->tmpNo;
					if(is_null($row->tmpNo)){
						$tmpNo	= 1;
					}
					else{
						$tmpNo 	= $tmpNo+1;
					}
				}
		
				$data 		= array('id'=> $id, 'agama' => $agama);
					
				$type		= "insert";
				$message	= 'Berhasil menambah Data';
				$table_id	= "id";
					
				$query				= $this->model->m_iud($type, $data, $table, $table_id, $id, $message);
				$resp['error'] 		= $query['error'];
				$resp['message']	= $query['message'];
				$resp['status']		= $query['status'];
					
				echo json_encode($resp);
				
			}
		}
		else
		{
				$data 		= array('isActive' => $ia);
					
				$type		= "update";
				$message	= 'Berhasil menghapus Data';
				$table_id	= "id";
				
				$query				= $this->model->m_iud($type, $data, $table, $table_id, $id, $message);
				$resp['error'] 		= $query['error'];
				$resp['message']	= $query['message'];
				$resp['status']		= $query['status'];
				
				echo json_encode($resp);
				
		}
		
		// $resp['error'] 		= '';
		// $resp['message']	= "Data anda gagal diproses, Mohon diulang kembali";
		// $resp['status']		= 'success';
		
		// echo json_encode($resp);
	}
}
