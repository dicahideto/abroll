<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class D_penduduk extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->model('ssp_class');
		$this->load->model('model');
		$this->load->library('template');
		$this->simple_login->cek_login();
		
		$this->sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);
    }

	public function index()
	{
		$data['code_prefix'] = '';
		
		$tambahan_cus	= "from 
						(
							select
							*
							from
							penduduk_master
							order by updated_date desc
						) as a order by updated_date desc";	
						
		$query3 = $this->db->query('SELECT * '.$tambahan_cus);

		$data['table3'] 		= $query3->result_array();
		
		
		$this->template->display('d_penduduk/v_d_penduduk', $data);
		
	}
	
	public function t_penduduk()
	{
		$data['code_prefix'] = '';
		
		$data['select_agama'] 	= $this->model->cetak('from agama_master where isActive in (1)');
		$data['select_pendidikan'] 	= $this->model->cetak('from pendidikan_master where isActive in (1)');
		$data['select_posisi'] 	= $this->model->cetak('from posisi_keluarga_master where isActive in (1)');
		
		$this->template->display('d_penduduk/v_t_penduduk', $data);
	}
	
	public function et_penduduk()
	{
		$idp = $this->input->get('idp');
		
		$data['code_prefix'] = '';
		
		$sql_disposisi 	= "
							from 
								(
									select
									*
									from
									penduduk_master
									where
									id_penduduk = '".$idp."'
								) as a";

		
		$data['list_data'] 			= $this->model->cetak_no_foreach($sql_disposisi);
		
		$data['select_agama'] 	= $this->model->cetak('from agama_master where isActive in (1)');
		$data['select_pendidikan'] 	= $this->model->cetak('from pendidikan_master where isActive in (1)');
		$data['select_posisi'] 	= $this->model->cetak('from posisi_keluarga_master where isActive in (1)');
		
		$this->template->display('d_penduduk/v_edit_penduduk', $data);
	}
	
	public function v_penduduk()
	{
		$idp = $this->input->get('idp');
		
		$data['code_prefix'] = '';
		
		$sql_disposisi 	= "
							from 
								(
									select
									*
									from
									penduduk_master
									where
									id_penduduk = '".$idp."'
								) as a";

		
		$data['list_data'] 			= $this->model->cetak_no_foreach($sql_disposisi);
		
		$this->template->display('d_penduduk/v_v_penduduk', $data);
	}
	
	public function save()
	{
		
		$code_jabatan				= $this->session->userdata('sessCodeBidang');
		
		$del				= $this->input->post('delete');
	
		$id						= $this->input->post('id');
		
		$nik						= $this->input->post('nik');
		$no_kk						= $this->input->post('no_kk');
		$name						= $this->input->post('name');
		$tempat_lahir				= $this->input->post('tempat_lahir');
		
		$get_tanggal_lahir			= $this->input->post('tanggal_lahir');
		$tanggal_lahir				= ($get_tanggal_lahir == "" || $get_tanggal_lahir == NULL ? NULL : date('Y-m-d', strtotime(str_replace("/","-", $get_tanggal_lahir))));
		
		$jenis_kelamin				= $this->input->post('jenis_kelamin');
		$agama						= $this->input->post('agama');
		$pendidikan					= $this->input->post('pendidikan');
		$posisi_keluarga			= $this->input->post('posisi_keluarga');
		$ibu_kandung				= $this->input->post('ibu_kandung');
		$profesi					= $this->input->post('profesi');
		
		$table				= 'penduduk_master';
		
		$udah_ada 			= false;
		$resp['edit'] 		= '';
		
		
		if ($del == ''){
			$q_cek	= $this->db->query("SELECT nik FROM ".$table." WHERE nik = '".$nik."'");
			$j_cek	= $q_cek->num_rows();
			if($j_cek == 1) {
				$udah_ada	= true;
			}
			if ($udah_ada)
			{
				$data 		= array('no_kk' => $no_kk, 'Nama_lengkap' => $name, 'tempatlahir' => $tempat_lahir, 'tanggallahir' => $tanggal_lahir, 'kelamin' => $jenis_kelamin, 'posisi_dikeluarga' => $posisi_keluarga, 'Agama' => $agama, 'Pendidikan_terakhir' => $pendidikan, 'profesi' => $profesi, 'nama_ibu' => $ibu_kandung);
					
				$type		= "update";
				$message	= 'Berhasil memperbarui Data Penduduk';
				$table_id	= "id_penduduk";
				
				$query				= $this->model->m_iud($type, $data, $table, $table_id , $id, $message);
				$resp['error'] 		= $query['error'];
				$resp['message']	= $query['message'];
				$resp['status']		= $query['status'];
				
				echo json_encode($resp);
				
			}
			else
			{
				$sql		 = "SELECT MAX(id_penduduk) as tmpNo FROM ".$table;
				$query		 = $this->db->query($sql);
				
				foreach($query->result() as $row){
					$tmpNo	= $row->tmpNo;
					if(is_null($row->tmpNo)){
						$tmpNo	= 1;
					}
					else{
						$tmpNo 	= $tmpNo+1;
					}
				}
		
				$data 		= array('id_penduduk'=> $tmpNo, 'nik' => $nik, 'no_kk' => $no_kk, 'Nama_lengkap' => $name, 'tempatlahir' => $tempat_lahir, 'tanggallahir' => $tanggal_lahir, 'kelamin' => $jenis_kelamin, 'posisi_dikeluarga' => $posisi_keluarga, 'Agama' => $agama, 'Pendidikan_terakhir' => $pendidikan, 'profesi' => $profesi, 'nama_ibu' => $ibu_kandung);
					
				$type		= "insert";
				$message	= 'Berhasil menyimpan Data Penduduk';
				$table_id	= "id_penduduk";
					
				$query				= $this->model->m_iud($type, $data, $table, $table_id , $id, $message);
				$resp['error'] 		= $query['error'];
				$resp['message']	= $query['message'];
				$resp['status']		= $query['status'];
					
				echo json_encode($resp);
				
			}
		}
		else
		{
			$data 		= "";
			$type		= "delete";
			$message	= 'Data berhasil dihapus';
			$table_id	= "id_penduduk";
			
			$query			= $this->model->m_iud($type, $data, $table, $table_id ,$id, $message);
			$resp['error'] 		= $query['error'];
			$resp['message']	= $query['message'];
			$resp['status']		= $query['status'];
			
			if ($query){
				$sql		= "ALTER TABLE ".$table." AUTO_INCREMENT = 1";
				$this->db->query($sql); 
			}	
			
			echo json_encode($resp);
				
		}
		
		// $resp['error'] 		= '';
		// $resp['message']	= "Data anda gagal diproses, Mohon diulang kembali";
		// $resp['status']		= 'success';
		
		// echo json_encode($resp);
	}
}
