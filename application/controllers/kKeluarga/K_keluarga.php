<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class K_keluarga extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->model('ssp_class');
		$this->load->model('model');
		$this->load->library('template');
		$this->simple_login->cek_login();
		
		$this->sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);
    }

	public function index()
	{
		$data['code_prefix'] = '';
		
		$tambahan_cus	= "from 
						(
							select
							*
							from
							m_user
						) as a";	
						
		$query3 = $this->db->query('SELECT * '.$tambahan_cus);

		$data['table3'] = $query3->result_array();
		
		$this->template->display('k_keluarga/v_k_keluarga', $data);
		
	}
}
