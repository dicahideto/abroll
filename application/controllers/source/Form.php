<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
		$this->load->model('ssp_class');
		$this->load->model('model');
		$this->load->library('template');
		$this->simple_login->cek_login();
		
		$this->sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);
    }

	public function index()
	{
		$data['code_prefix'] = '';
		
		$select_propinsi			= "m_user where isActive=1";
		$data['select_prop'] 		= $this->model->getAll($select_propinsi);
		
		$this->template->display('source/v_form', $data);
		
	}
}
