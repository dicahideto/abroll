<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public $data = array('pesan'=>'');
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('m_login', 'm_login', TRUE);
	}
	
	public function index()
	{
		// $this->template->display('template');
		if($this->session->userdata('login'))
		{
			header("location:javascript://history.go(-1)");
		}
		else
		{
			$this->load->view('v_login.php');
		}
	}        
	
	public function check()
	{
		$username = trim($this->input->post('username'));
		$password = base64_encode(trim($this->input->post('password')));
		// $password = base64_encode(trim($this->input->post('password')));
		
		$check         = $this->m_login->check_login($username, $password);

		if($check)
		{
			$role_cek	= $this->db->query("SELECT * FROM m_user WHERE username = '".$username."' and password = '".$password."' and isActive ='1' limit 1");
			$jrole_cek	= $role_cek->num_rows();
			$drole_cek	= $role_cek->row();
			
			// if ($drole_cek > 0){
				// $name				= $drole_cek->name;
				// $role				= $drole_cek->role;
				// $foto				= $drole_cek->foto;
				
				// if ($role == 1)
				// {
					// $rolea = 'Administration';
				// }
				// else if ($role == 2)
				// {
					// $rolea = 'Finance';
				// }
			
				$idUser						= $drole_cek->code;
				
				// $update_status_login         = $this->m_login->update_status_login($idUser);
			// }
			
			$this->session->set_flashdata('message_success', 'Login berhasil');
			$this->session->set_userdata('usernameGA', $username );
			$this->session->set_userdata('idUser', $idUser );
			
			// $this->session->set_userdata('roleGA', $role );
			
			// if ($role == 3)
			// {
				// $this->session->set_userdata('nameGA', 'Dicahideto' );
				// $this->session->set_userdata('roleaGA', 'Admin' );
				// $this->session->set_userdata('fotoGA', 'superadmin.jpg' );
			// }
			// else
			// {
				// $this->session->set_userdata('nameGA', $name );
				// $this->session->set_userdata('roleaGA', $rolea );
				// $this->session->set_userdata('fotoGA', $foto );
			// }
			
			$this->session->set_userdata('login', TRUE);
			// $session = $this->session->userdata('db');
			// print_r($db);
			redirect('Welcome');
		}
		else
		{
			$this->session->set_flashdata('message_error', 'Combination Username and Password is Wrong');
		} 
		redirect('login');
	}
	
	public function logout()
	{
		// $this->m_login->clear_session($this->session->userdata('idUser'));
		// $this->session->unset_userdata(array('usernameHMS'=>'', 'login'=> FALSE));
		$this->session->sess_destroy();
		redirect('login');
	
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */