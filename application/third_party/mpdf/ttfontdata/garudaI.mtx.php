<?php
$name='Garuda-Oblique';
$type='TTF';
$desc=array (
  'CapHeight' => 716,
  'XHeight' => 518,
  'FontBBox' => '[-522 -589 1151 1288]',
  'Flags' => 68,
  'Ascent' => 1284,
  'Descent' => -589,
  'Leading' => 0,
  'ItalicAngle' => -12,
  'StemV' => 87,
  'MissingWidth' => 766,
);
$unitsPerEm=1000;
$up=-32;
$ut=10;
$strp=258;
$strs=49;
$ttffile='C:/xampp/htdocs/mpdf/ttfonts/Garuda-Oblique.ttf';
$TTCfontID='0';
$originalsize=57412;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='garudaI';
$panose=' 0 0 2 b 6 4 2 2 2 2 2 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 840, -160, 0
// usWinAscent/usWinDescent = 1284, -591
// hhea Ascent/Descent/LineGap = 1284, -591, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>