<?php
$name='Garuda-BoldOblique';
$type='TTF';
$desc=array (
  'CapHeight' => 700,
  'XHeight' => 521,
  'FontBBox' => '[-606 -591 1161 1337]',
  'Flags' => 262212,
  'Ascent' => 1284,
  'Descent' => -591,
  'Leading' => 0,
  'ItalicAngle' => -12,
  'StemV' => 165,
  'MissingWidth' => 766,
);
$unitsPerEm=1000;
$up=-37;
$ut=20;
$strp=258;
$strs=49;
$ttffile='C:/xampp/htdocs/visi/application/third_party/mpdf/ttfonts/Garuda-BoldOblique.ttf';
$TTCfontID='0';
$originalsize=57460;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='garudaBI';
$panose=' 0 0 2 b 7 4 2 2 2 2 2 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 872, -128, 0
// usWinAscent/usWinDescent = 1284, -591
// hhea Ascent/Descent/LineGap = 1284, -591, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>