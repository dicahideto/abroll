<?php
$name='DejaVuSansMono-Bold';
$type='TTF';
$desc=array (
  'CapHeight' => 729,
  'XHeight' => 547,
  'FontBBox' => '[-447 -394 731 1052]',
  'Flags' => 262149,
  'Ascent' => 928,
  'Descent' => -236,
  'Leading' => 0,
  'ItalicAngle' => 0,
  'StemV' => 165,
  'MissingWidth' => 602,
);
$unitsPerEm=2048;
$up=-63;
$ut=44;
$strp=259;
$strs=50;
$ttffile='/var/www/html/aang/application/third_party/mpdf/ttfonts/DejaVuSansMono-Bold.ttf';
$TTCfontID='0';
$originalsize=318392;
$sip=false;
$smp=false;
$BMPselected=true;
$fontkey='dejavusansmonoB';
$panose=' 0 0 2 b 7 9 3 6 4 2 2 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 760, -240, 200
// usWinAscent/usWinDescent = 928, -236
// hhea Ascent/Descent/LineGap = 928, -236, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>