<?php
$name='ArialNarrow-BoldItalic';
$type='TTF';
$desc=array (
  'Ascent' => 936,
  'Descent' => -212,
  'CapHeight' => 936,
  'Flags' => 262212,
  'FontBBox' => '[-204 -307 1000 1107]',
  'ItalicAngle' => -9.6999969482422,
  'StemV' => 165,
  'MissingWidth' => 778,
);
$up=-106;
$ut=105;
$ttffile='F:/work/Source/Web/php/visi/application/third_party/mpdf/ttfonts/ARIALNBI.ttf';
$TTCfontID='0';
$originalsize=178316;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='arial-narrowBI';
$panose=' 8 5 2 b 7 6 2 2 2 a 2 4';
$haskerninfo=false;
$unAGlyphs=false;
?>