<?php
$name='ArialNarrow-BoldItalic';
$type='TTF';
$desc=array (
  'CapHeight' => 716,
  'XHeight' => 519,
  'FontBBox' => '[-204 -307 1000 1107]',
  'Flags' => 262212,
  'Ascent' => 936,
  'Descent' => -212,
  'Leading' => 0,
  'ItalicAngle' => -9.6999969482421875,
  'StemV' => 165,
  'MissingWidth' => 778,
);
$unitsPerEm=2048;
$up=-106;
$ut=105;
$strp=259;
$strs=50;
$ttffile='C:/xampp/htdocs/visi/application/third_party/mpdf/ttfonts/ARIALNBI.ttf';
$TTCfontID='0';
$originalsize=178316;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='arialnarrowBI';
$panose=' 8 5 2 b 7 6 2 2 2 a 2 4';
$haskerninfo=true;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 728, -210, 131
// usWinAscent/usWinDescent = 936, -212
// hhea Ascent/Descent/LineGap = 936, -212, 0
$useOTL=0x0000;
$rtlPUAstr='';
$kerninfo=array (
  49 => 
  array (
    49 => -55,
  ),
  65 => 
  array (
    84 => -74,
    86 => -74,
    87 => -55,
    89 => -91,
    118 => -37,
    119 => -18,
    121 => -37,
    8217 => -55,
    354 => -74,
    356 => -74,
  ),
  70 => 
  array (
    44 => -110,
    46 => -110,
    65 => -55,
    260 => -55,
  ),
  76 => 
  array (
    84 => -74,
    86 => -74,
    87 => -55,
    89 => -91,
    121 => -37,
    8217 => -55,
    354 => -74,
    356 => -74,
  ),
  80 => 
  array (
    44 => -128,
    46 => -128,
    65 => -74,
    260 => -74,
  ),
  82 => 
  array (
    86 => -18,
    87 => -18,
    89 => -37,
  ),
  84 => 
  array (
    44 => -110,
    46 => -110,
    58 => -110,
    65 => -74,
    79 => -18,
    97 => -74,
    99 => -74,
    101 => -74,
    105 => -18,
    111 => -74,
    114 => -55,
    115 => -74,
    117 => -74,
    119 => -74,
    121 => -74,
    260 => -74,
    261 => -74,
    281 => -74,
    336 => -18,
  ),
  86 => 
  array (
    44 => -91,
    46 => -91,
    58 => -55,
    65 => -74,
    97 => -55,
    101 => -55,
    105 => -18,
    111 => -74,
    114 => -55,
    117 => -37,
    121 => -37,
    260 => -74,
    261 => -55,
    281 => -55,
  ),
  87 => 
  array (
    44 => -55,
    46 => -55,
    58 => -18,
    65 => -55,
    97 => -37,
    101 => -18,
    105 => -8,
    111 => -18,
    114 => -18,
    117 => -18,
    121 => -18,
    260 => -55,
    261 => -37,
    281 => -18,
  ),
  89 => 
  array (
    44 => -110,
    46 => -110,
    58 => -74,
    65 => -91,
    97 => -55,
    101 => -55,
    105 => -37,
    111 => -74,
    112 => -55,
    113 => -74,
    117 => -55,
    118 => -55,
    260 => -91,
    261 => -55,
    281 => -55,
  ),
  102 => 
  array (
    8217 => 18,
  ),
  114 => 
  array (
    44 => -55,
    46 => -55,
    8217 => 37,
  ),
  118 => 
  array (
    44 => -74,
    46 => -74,
  ),
  119 => 
  array (
    44 => -37,
    46 => -37,
  ),
  121 => 
  array (
    44 => -74,
    46 => -74,
  ),
  8216 => 
  array (
    8216 => -37,
  ),
  8217 => 
  array (
    115 => -37,
    8217 => -37,
  ),
  260 => 
  array (
    84 => -74,
    86 => -74,
    87 => -55,
    89 => -91,
    118 => -37,
    119 => -18,
    8217 => -55,
    354 => -74,
    356 => -74,
  ),
  313 => 
  array (
    84 => -74,
    86 => -74,
    87 => -55,
    89 => -91,
    121 => -37,
    8217 => -55,
    354 => -74,
    356 => -74,
  ),
  340 => 
  array (
    86 => -18,
    87 => -18,
    89 => -37,
  ),
  341 => 
  array (
    44 => -55,
    46 => -55,
    8217 => 37,
  ),
  344 => 
  array (
    86 => -18,
    87 => -18,
    89 => -37,
  ),
  345 => 
  array (
    44 => -55,
    46 => -55,
    8217 => 37,
  ),
  354 => 
  array (
    44 => -110,
    46 => -110,
    58 => -110,
    65 => -74,
    79 => -18,
    97 => -74,
    99 => -74,
    101 => -74,
    105 => -18,
    111 => -74,
    114 => -55,
    115 => -74,
    117 => -74,
    119 => -74,
    121 => -74,
    260 => -74,
    261 => -74,
    281 => -74,
    336 => -18,
  ),
  356 => 
  array (
    44 => -110,
    46 => -110,
    58 => -110,
    65 => -74,
    79 => -18,
    97 => -74,
    99 => -74,
    101 => -74,
    105 => -18,
    111 => -74,
    114 => -55,
    115 => -74,
    117 => -74,
    119 => -74,
    121 => -74,
    260 => -74,
    261 => -74,
    281 => -74,
    336 => -18,
  ),
);
?>