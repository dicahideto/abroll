<?php
$name='1979-Dot-Matrix';
$type='TTF';
$desc=array (
  'CapHeight' => 875,
  'XHeight' => 875,
  'FontBBox' => '[0 -250 1000 875]',
  'Flags' => 4,
  'Ascent' => 875,
  'Descent' => -250,
  'Leading' => 0,
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 125,
);
$unitsPerEm=1024;
$up=100;
$ut=50;
$strp=199;
$strs=50;
$ttffile='/var/www/html/aang/application/third_party/mpdf/ttfonts/1979_dot_matrix.ttf';
$TTCfontID='0';
$originalsize=140300;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='matrix';
$panose=' 0 0 0 0 4 0 0 0 0 0 0 0';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 875, 0, 0
// usWinAscent/usWinDescent = 875, -250
// hhea Ascent/Descent/LineGap = 875, -250, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>