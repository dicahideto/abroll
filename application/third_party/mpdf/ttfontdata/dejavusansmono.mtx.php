<?php
$name='DejaVuSansMono';
$type='TTF';
$desc=array (
  'CapHeight' => 729,
  'XHeight' => 547,
  'FontBBox' => '[-558 -375 718 1042]',
  'Flags' => 5,
  'Ascent' => 928,
  'Descent' => -236,
  'Leading' => 0,
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 602,
);
$unitsPerEm=2048;
$up=-63;
$ut=44;
$strp=259;
$strs=50;
$ttffile='/var/www/html/aang/application/third_party/mpdf/ttfonts/DejaVuSansMono.ttf';
$TTCfontID='0';
$originalsize=335068;
$sip=false;
$smp=false;
$BMPselected=true;
$fontkey='dejavusansmono';
$panose=' 0 0 2 b 6 9 3 8 4 2 2 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 760, -240, 200
// usWinAscent/usWinDescent = 928, -236
// hhea Ascent/Descent/LineGap = 928, -236, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>