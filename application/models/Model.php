<?php
	// Director     : Rievky Ardica
	// Last Update	: 4/sep/2017
	
class Model extends CI_Model {
	function m_iud($type, $data, $table, $table_id ,$primarykey, $message) {
		if ($type == 'delete'){
			$this->db->where($table_id, $primarykey);
			$this->db->delete($table); 
		}
		if ($type == 'update'){
			$this->db->where($table_id, $primarykey);
			$this->db->update($table, $data); 	
		}
		if ($type == 'insert'){
			$this->db->insert($table, $data); 	
		}
		// $errNo   = $this->db->_error_number();
		// $errMess = $this->db->_error_message();
		
		// $error  = $this->db->error();
		// $num 	= $error->code;
		// $mess 	= $error->message;
		
		
		// if($mess){
			// $resp['error'] 		= '';
			// $resp['message']	= $mess;
			// $resp['status']		= "error";
			// if ($num == 1062){
				// $resp['message']	= 'Data double !';
			// }
			// if($num == 1451){
				// $this->db->where($table_id, $primarykey);
				// $this->db->update($table, $data); 
				// $resp['error'] 		= '';
				// $resp['status'] 	= 'success';
				// $resp['id'] 		= $this->db->insert_id();
				// $resp['message'] 	= $message;
			// }
		// }
		// else{
			$resp['error'] 		= '';
			$resp['status'] 	= 'success';
			$resp['id'] 		= $this->db->insert_id();
			$resp['message'] 	= $message;
				
		// }
		
		return $resp ;
	}
	
	
	function m_iud_dic($type, $data, $table, $focus_id , $focus_id_II, $id, $id_II, $message) {
		if ($type == 'delete'){
			// $this->db->query("delete FROM temp_po_detail WHERE po_code = '".$id."' AND item_code = '".$id_II."'");
			$this->db->where($focus_id, $id);
			$this->db->where($focus_id_II, $id_II);
			$this->db->delete($table); 
		}
		if ($type == 'update'){
		
			$this->db->where($focus_id, $id);
			$this->db->where($focus_id_II, $id_II);
			$this->db->update($table, $data); 	
				
		}
		if ($type == 'insert'){
			$this->db->insert($table, $data); 	
		}
		// $errNo   = $this->db->_error_number();
		// $errMess = $this->db->_error_message();
		
		// if($errMess){
			// $resp['error'] 		= $errMess;
			// $resp['message']	= "Number of error : " .$errNo. " => Detail Error : " .$errMess;
			// $resp['status']		= "error";
			// if ($errNo == 1062){
				// $resp['message']	= 'Data sudah pernah di input !!';
			// }
			// if($errNo == 1451){
				// $this->db->where($table_id, $primarykey);
				// $this->db->update($table, $data); 
				// $resp['error'] 		= '';
				// $resp['status'] 	= 'success';
				// $resp['id'] 		= $this->db->insert_id();
				// $resp['message'] 	= $message;
			// }
		// }
		// else{
			$resp['error'] 		= '';
			$resp['status'] 	= 'success';
			$resp['id'] 		= $this->db->insert_id();
			$resp['message'] 	= $message;
				
		// }
		
		return $resp ;
	}
	
	
	function m_iud_dic_2($type, $data, $table, $focus_id , $focus_id_II, $focus_id_III, $id, $id_II, $id_III, $message) {
		if ($type == 'delete'){
			// $this->db->query("delete FROM temp_po_detail WHERE po_code = '".$id."' AND item_code = '".$id_II."'");
			$this->db->where($focus_id, $id);
			$this->db->where($focus_id_II, $id_II);
			$this->db->delete($table); 
		}
		if ($type == 'update'){
		
			$this->db->where($focus_id, $id);
			$this->db->where($focus_id_II, $id_II);
			$this->db->where($focus_id_III, $id_III);
			$this->db->update($table, $data); 	
				
		}
		if ($type == 'insert'){
			$this->db->insert($table, $data); 	
		}
		// $errNo   = $this->db->_error_number();
		// $errMess = $this->db->_error_message();
		
		// if($errMess){
			// $resp['error'] 		= $errMess;
			// $resp['message']	= "Number of error : " .$errNo. " => Detail Error : " .$errMess;
			// $resp['status']		= "error";
			// if ($errNo == 1062){
				// $resp['message']	= 'Data sudah pernah di input !!';
			// }
			// if($errNo == 1451){
				// $this->db->where($table_id, $primarykey);
				// $this->db->update($table, $data); 
				// $resp['error'] 		= '';
				// $resp['status'] 	= 'success';
				// $resp['id'] 		= $this->db->insert_id();
				// $resp['message'] 	= $message;
			// }
		// }
		// else{
			$resp['error'] 		= '';
			$resp['status'] 	= 'success';
			$resp['id'] 		= $this->db->insert_id();
			$resp['message'] 	= $message;
				
		// }
		
		return $resp ;
	}
	
	
	function getNo($nomor, $panjang) {
		$q = $this->db->query("SELECT `format_nomor`($nomor, $panjang) as nomor ");
		return $q->row();	
	}
	
	
	//======================================================== INSERT =================================================================
	public function insertMail($data)
	{
		$this->db->insert('t_email',$data);
		if($this->db->affected_rows() > 0){
			return $this->db->insert_id();
		}
		return false;
	}
	
	public function insert_data($data)
	{
		$query = $this->db->insert($this->table, $data);
		return $query;
	}
	
	public function addDataMenu($position_id, $data)
	{
		$this->db->where('m_user_group', $position_id);
		$this->db->delete('m_user_group_menu');
		
		$this->db->insert_batch('m_user_group_menu',$data);
		if($this->db->affected_rows() > 0)
		{
			return true;
		}
		return false;
	}
	
	
	//======================================================== HITUNG DATA =================================================================
	public function hitung_count($table, $table_id, $primarykey)
	{
		$query = $this->db->query("
									select 
									count(*) as total
									from
									t_data_pribadi
									where 
									status_proses = 1");
		return $query->result();
		
	}
	//======================================================== / HITUNG DATA =================================================================
	
	//======================================================== SELECT DATA =================================================================
	
	function getAll($tabel) {
		$q = $this->db->query("SELECT * FROM $tabel");
		return $q->result();
	}
	
	public function select_data_by_id($table_id, $primarykey, $table)
	{
		// $this->db->order_by('created_date', 'desc');
		
		$this->db->where($table_id, $primarykey);
		$query = $this->db->get($table);
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		return null;
	}
	
	
	public function select_data_custom($table_id, $primarykey, $table)
	{
		$query = $this->db->query("
						select * from ".$table." WHERE ".$table_id." = '$primarykey'
					");
		return $query->result();
	}
	
	public function select_data_custom_II($where, $table)
	{
		$query = $this->db->query("
						select * from ".$table." WHERE ".$where."
					");
		return $query->result();
	}
	
	
	public function selectById($field, $code, $table)
	{
		$this->db->where($field, $code);
		$query = $this->db->get($table);
		if($query->num_rows() > 0)
			return $query->result();
		
		return null;
	}
	
	//======================================================== INSERT =================================================================
	
	public function select_dropdown($code, $table)
	{
		$this->db->select($code);
		$this->db->from($table);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		return false;
	}
	
	
	public function select_dropdown_II($code, $nama_field, $value_field, $table)
	{
		$this->db->select($code);
		$this->db->where($nama_field, $value_field);
		$this->db->from($table);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		return false;
	}
	
	public function select_dropdown_in($code, $nama_field, $table)
	{
		$this->db->select($code);
		$this->db->where($nama_field);
		$this->db->from($table);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		return false;
	}
	
	public function select_dropdown_III($code, $nama_field, $value_field, $nama_field2, $value_field3, $table)
	{
		$this->db->select($code);
		$this->db->where($nama_field, $value_field);
		$this->db->where($nama_field2, $value_field3);
		$this->db->from($table);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		return false;
	}
	
	
	//======================================================== Cetak =================================================================
	
	public function cetak($where)
	{
		$query = $this->db->query("
					SELECT * 
					
					".$where."
				");
		
		
		return $query->result();
	}
	
	public function cetak_no_foreach($where)
	{
		$query = $this->db->query("
					SELECT * 
					
					".$where."
				");
		
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		return null;
	}
}

?>