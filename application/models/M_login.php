<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_login extends CI_Model
{
	public $db_tabel_login = "user";
		
	public function load_form_rules()
	{
		$this->load->library('form_validation');
		$form_rules = array
							(
								array 
									(
										'field' => 'username',
										'label' => 'Username',
										'rules' => 'required'
									),
								array 
									(
										'field' => 'password',
										'label' => 'Password',
										'rules' => 'required'
									),
							);
		return $form_rules;
	}
	
	public function check_login($username, $password)
	{
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$this->db->where('isActive', 1);
		$query = $this->db->get('m_user');
		if($query->num_rows() == 1)
		{
			return true;
		}
		return false;
	}
	
	
	public function update_status_login($idUser)
	{
		$data_sess = array(
					   'isLogin' => 1
					);
					
		$this->db->where('code', $idUser);
		$this->db->update('m_user', $data_sess); 
		
		if($this->db->affected_rows() > 0)
		{
			return true;
		}
		return false;
	}
	
	
	function checkAlreadyLogin($username)
	{
		$this->db->where('isLogin',1);
		$this->db->where('username',$username);
		$this->db->from('m_user');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return true;
		}
		return false;
	}
	
	public function clear_session($idUser)
	{
		$data_sess = array(
					   'isLogin' => 0
					);
					
		$this->db->where('code', $idUser);
		$this->db->update('m_user', $data_sess); 
		
		if($this->db->affected_rows() > 0)
		{
			return true;
		}
		return false;
	}
}