<?php if(! defined('BASEPATH')) exit('Akses langsung tidak diperbolehkan'); ?>

<?php
class Simple_login{
	// SET SUPER GLOBAL
	 var $CI = NULL;
	 public function __construct() {
	 $this->CI =& get_instance();
	 }
 
	 // Proteksi halaman
	 public function cek_login() {
			 if($this->CI->session->userdata('login') == FALSE) {
			 $this->CI->session->set_flashdata('sukses','Anda belum login');
			 redirect(base_url('login'));
			 }
	 }
	 
}

?>