<?php
class Custom_lib
{
	private $ci;
	
	public function __construct()
	{
		$this->ci =& get_instance();
	}
	
	public function new_id($initial, $table)
	{
		$initial	= strtoupper($initial);
		$month 		= date('m');
		$years 		= date('Y');
		$year 		= date('y');
		
		$this->ci->db->where('MONTH(created_date)', $month);
		$this->ci->db->where('YEAR(created_date)', $years);
		$query = $this->ci->db->get($table);
		$total_row = $query->num_rows();

		$total_row++;
		
		$increment 	= sprintf('%06d', $total_row);
		
		$new_id = "$initial/$year/$month/$increment";

		return $new_id;
	}

	// dicahideto 
	// 28 Agus 2017
	
	public function new_id_dica($initial, $table)
	{
		$initial	= strtoupper($initial);
		$month 		= date('m');
		$years 		= date('Y');
		$year 		= date('y');
		
			function ConverToRoman($num){
				$n = intval($num); 
				$res = ''; 

				//array of roman numbers
				$romanNumber_Array = array( 
					'M'  => 1000, 
					'CM' => 900, 
					'D'  => 500, 
					'CD' => 400, 
					'C'  => 100, 
					'XC' => 90, 
					'L'  => 50, 
					'XL' => 40, 
					'X'  => 10, 
					'IX' => 9, 
					'V'  => 5, 
					'IV' => 4, 
					'I'  => 1); 

				foreach ($romanNumber_Array as $roman => $number){ 
					//divide to get  matches
					$matches = intval($n / $number); 

					//assign the roman char * $matches
					$res .= str_repeat($roman, $matches); 

					//substract from the number
					$n = $n % $number; 
				} 

				// return the result
				return $res; 
			} 
	
		$month_convert = ConverToRoman($month);
		
		$this->ci->db->where('MONTH(created_date)', $month);
		$this->ci->db->where('YEAR(created_date)', $years);
		$query = $this->ci->db->get($table);
		$total_row = $query->num_rows();

		$total_row++;

		$increment 	= sprintf('%03d', $total_row);
		
		$new_id = "$initial-$increment/$month_convert/$years";

		return $new_id;
	}
	
	
	public function new_id_dic_I($initial, $table)
	{
		$initial	= strtoupper($initial);
		$month 		= date('m');
		$years 		= date('Y');
		$year 		= date('y');
		
		// $this->ci->db->where('MONTH(created_date)', $month);
		$this->ci->db->where('YEAR(created_date)', $years);
		$query = $this->ci->db->get($table);
		$total_row = $query->num_rows();

		$total_row++;

		$increment 	= sprintf('%03d', $total_row);
		
		$new_id = "$initial-$increment/";

		return $new_id;
	}
	
	
	
	public function new_id_dica_II($table)
	{
		$month 		= date('m');
		$years 		= date('Y');
		$year 		= date('y');
		
			function ConverToRoman($num){
				$n = intval($num); 
				$res = ''; 

				//array of roman numbers
				$romanNumber_Array = array( 
					'M'  => 1000, 
					'CM' => 900, 
					'D'  => 500, 
					'CD' => 400, 
					'C'  => 100, 
					'XC' => 90, 
					'L'  => 50, 
					'XL' => 40, 
					'X'  => 10, 
					'IX' => 9, 
					'V'  => 5, 
					'IV' => 4, 
					'I'  => 1); 

				foreach ($romanNumber_Array as $roman => $number){ 
					//divide to get  matches
					$matches = intval($n / $number); 

					//assign the roman char * $matches
					$res .= str_repeat($roman, $matches); 

					//substract from the number
					$n = $n % $number; 
				} 

				// return the result
				return $res; 
			} 
	
		$month_convert = ConverToRoman($month);
		
		$new_id = "/$month_convert/$years";

		return $new_id;
	}
	
	
	//for inventory_tranfer_entry
	public function new_id_in_transf($initial, $table)
	{
		$initial	= strtoupper($initial);
		$month 		= date('m');
		$years 		= date('Y');
		$year 		= date('y');
		
		// $this->ci->db->where('MONTH(created_date)', $month);
		$this->ci->db->where('YEAR(created_date)', $years);
		$query = $this->ci->db->get($table);
		$total_row = $query->num_rows();

		$total_row++;

		$increment 	= sprintf('%05d', $total_row);
		
		$new_id = "/$initial/$years/$month/$increment";

		return $new_id;
	}
	
	//dic_increment
	public function new_id_for_increment_stat($initial, $table){
		$initial	= strtoupper($initial);
		$month 		= date('m');
		$years 		= date('Y');
		$year 		= date('y');
		
		// $this->ci->db->where('MONTH(created_date)', $month);
		$this->ci->db->where('YEAR(created_date)', $years);
		$query = $this->ci->db->get($table);
		$total_row = $query->num_rows();

		$total_row++;

		$increment 	= sprintf('%03d', $total_row);
		
		$abcd = "$increment";

		return $abcd;
		
		
		
	}
}
